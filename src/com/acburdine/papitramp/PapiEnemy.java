package com.acburdine.papitramp;

import static org.lwjgl.opengl.GL11.glRectd;

import org.newdawn.slick.Color;

import com.acburdine.arcademachine.ArcadeMachine;
import com.acburdine.arcademachine.entities.AbstractMoveableEntity;
import com.acburdine.arcademachine.entities.Entity;


public class PapiEnemy extends AbstractMoveableEntity {

	private Color c;
	private int score;
	private int buffer=3;
	private final double DIFFICULITY=10;
	public PapiEnemy(int choice){
		super(-80,0,50,50);
		double speed;
		int yPos,length=50;
		switch(choice){
			case 1: 
				score=130;
				yPos=145;
				c=Color.pink;
				speed=1.5;
				break;
			case 2: 
				score=170;
				length=120;
				yPos=225;
				c=Color.orange;
				speed=1.3;
				break;
			case 3: 
				score=250;
				yPos=300;
				c=Color.red;
				speed=1.2;
				break;
			default: 
				score=100;
				yPos=140;
				c=Color.white;
				speed=1;
				break;
		}
		boolean left= Math.random()>.5;
		int xPos;
		if(left){
			xPos=ArcadeMachine.WIDTH+20;
			speed*=-1;
		}else{
			
			xPos=-60;
			
		}
		setX(xPos);
		setY(ArcadeMachine.HEIGHT-yPos);
		setDY(0);
		setDX(speed/10);
	}
	
	
	public void draw() {
		Color.black.bind();
		glRectd(x-buffer,y-buffer,x+width+buffer,y+height+buffer);
		c.bind();
		glRectd(x,y,x+width,y+height);
		Color.black.bind();

	}
	
	public int getScore(){
		return score;
	}

}
