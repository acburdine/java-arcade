package com.acburdine.papitramp;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.acburdine.arcademachine.ArcadeMachine;
import com.acburdine.arcademachine.Game;
import com.acburdine.arcademachine.MasterMenu;
import com.acburdine.arcademachine.entities.Ballv2;
import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.IFont;
import com.acburdine.arcademachine.utils.Sound;
import com.acburdine.arcademachine.utils.SoundLoader;

public class PapiTramp extends Game {

	private Ballv2 papi;
	private ArrayList<PapiEnemy> npcs;
	private ArrayList<PapiCombo> comboList=new ArrayList<PapiCombo>(10);
	private long spernTime= 5000,timeSpawned, timeStart,timeTilCarrots=10000;//who knows what value to put here
	private double maxSpeed=.2,jumpSpeed=.6,acceleration=.03;//who knows what value to put here
	private Sound bounce,jump,go,pause,death;
	private boolean paused=false, hasNotJumped=true,die=false;
	private int score=0, combo=0;
	private IFont font = FontLoader.getFont("font_60");
	private TrueTypeFont userFont = font.getFont();
	private IFont sfont = FontLoader.getFont("minecraftia_24");
	private TrueTypeFont suserFont = sfont.getFont();


	public PapiTramp() {
		timeStart=ArcadeMachine.getTime();
	}

	@Override
	protected void setUpEntites() {
		papi= new Ballv2(ArcadeMachine.WIDTH/2,ArcadeMachine.HEIGHT-165, 25);
		npcs= new ArrayList<PapiEnemy>();
		papi.setColor(Color.red);
	}

	@Override
	protected void setUpSound() {
		pause = SoundLoader.getSound("mariosounds_Pause");
		bounce= SoundLoader.getSound("mariosounds_coin");
		jump= SoundLoader.getSound("mariosounds_Mario Jump");
		go= SoundLoader.getSound("mariosounds_Mario Riff");
		death= SoundLoader.getSound("mariosounds_Death");

	}

	@Override
	public void render() {
		glClear(GL_COLOR_BUFFER_BIT); 
		if(paused){
			//pause menu
			return;
		}

		if(!die){
		logic(ArcadeMachine.getDelta());
		}
//		glPushAttrib(GL_ENABLE_BIT); 
//		short s = (short) 0xAAAA;
//		glLineStipple(4, s);
//		glEnable(GL_LINE_STIPPLE);
//		
//		
//		glBegin(GL_LINES);
//		glVertex3i(0, 30, 1);
//		glVertex3i(ArcadeMachine.WIDTH, 30, 1);
//		glEnd();
//		glPopAttrib();
		

		glBindTexture(GL_TEXTURE_2D, 0);
		Color.blue.bind();
		glRectd(0,0,ArcadeMachine.WIDTH,ArcadeMachine.HEIGHT);
		Color.green.bind();
		glRectd(0,ArcadeMachine.HEIGHT-100,ArcadeMachine.WIDTH,ArcadeMachine.HEIGHT);
		//background: Green Floor
		//Score: Upper Left
		glBindTexture(GL_TEXTURE_2D, font.getTextureID());
		String score = this.score+"  ";
		userFont.drawString(ArcadeMachine.WIDTH/2, 50, score, Color.white);
		
		
		glBindTexture(GL_TEXTURE_2D, 0);
		Color.green.bind();
		for(PapiEnemy enemy: npcs){
			enemy.draw();
		}

		Color.red.bind();
		papi.draw();

		glBindTexture(GL_TEXTURE_2D, font.getTextureID());
		for(int i= comboList.size()-1;i>=0; i--){
			PapiCombo pc= comboList.get(i);
			if(pc.getTimeOfDeath()<ArcadeMachine.getTime()){
				comboList.remove(i);
				continue;
			}
			if(!(pc.getCombo()==1)){
				suserFont.drawString((float) pc.getX(), (float) pc.getY()-pc.dy, "Combo: "+pc.getCombo()+"*"+pc.getScore(), Color.white);
			}else{
				System.out.println("First!");
				suserFont.drawString((float) pc.getX(), (float) pc.getY()-pc.dy, ""+pc.getScore(), Color.white);
			}
			pc.dy++;
		}
		
		if(die){
			glBindTexture(GL_TEXTURE_2D, sfont.getTextureID());
			suserFont.drawString(30, 150, "You lose! Enter to restart", new Color(((int)Math.random()*150)+100,(int)(Math.random()*150)+100,(int)(Math.random()*150)+100));
		}
		

	}

	
	@Override
	public void input() {
		
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)){
			if(die){
				resetGame();
			}
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			if(paused==false){
				paused=true;
				pause.playSound();
			} 
		}

		if(Keyboard.isKeyDown(Keyboard.KEY_R)){
			if(paused==true){
				paused=false;
				pause.playSound();
			} 
		}

		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
			ArcadeMachine.setState(new MasterMenu());
		}
		boolean left= (Keyboard.isKeyDown(Keyboard.KEY_LEFT)||Keyboard.isKeyDown(Keyboard.KEY_A));
		boolean right= (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)||Keyboard.isKeyDown(Keyboard.KEY_D));
		boolean up= (Keyboard.isKeyDown(Keyboard.KEY_UP)||Keyboard.isKeyDown(Keyboard.KEY_W));
		//boolean down= (Keyboard.isKeyDown(Keyboard.KEY_DOWN)||Keyboard.isKeyDown(Keyboard.KEY_S));

		if(left){
			papi.setDX(-(maxSpeed));
		}else if(right){
			papi.setDX((maxSpeed));
		}else{
			papi.setDX(0);
		}
		if(up&&hasNotJumped){
			jump.playSound();
			papi.setDY(-jumpSpeed);
			hasNotJumped=false;
		}
		//System.out.printf("X:%5f  Y:$5f  dX:%5f  dY:$5f \n",papi.getX(),papi.getY(), papi.getDX(),papi.getDY());

	}

	private void resetGame() {
		setUpEntites();
		spernTime= 5000;
		die=false;
		
	}

	public void logic(int delta) {
		for(PapiEnemy enemy: npcs){
			enemy.update(delta);
		}

		papi.update(delta);
		
		if(papi.getX()<5)papi.setX(5);
		if(papi.getX()+papi.getWidth()+5>ArcadeMachine.WIDTH)papi.setX(ArcadeMachine.WIDTH-papi.getWidth()-5);
		if(timeSinceSpwan()>spernTime){
			spawnEnemy();
		}

		if(papi.getY()>ArcadeMachine.HEIGHT-150){ //if papi on the ground
			papi.setY(ArcadeMachine.HEIGHT-150);
			if(!hasNotJumped) hasNotJumped=true;
			papi.setDY(0);
			combo=0;
		}else{ //papi in the air
			papi.setDY(papi.getDY()+acceleration);
		}

		
		//collision detection
		boolean jump=false;
		for(int i=npcs.size()-1; i>=0; i--){
			if(npcs.get(i).intersects(papi)){
				if(!hasNotJumped||papi.getDY()>0&&papi.getY()+papi.getHeight()+5>npcs.get(i).getY()){
					//npcs.get(i).die();
					if(combo>20)combo=20;
					updateScore(npcs.get(i).getScore(), i);
					npcs.remove(i);
					jump=true;
					
				}else{
					papiDie();
				}
			}
		}
		if(jump){
			papi.setDY(-jumpSpeed);
			bounce.playSound();
		}

	}

	private void papiDie() {
		death.playSound();
		die=true;
		score=0;
		
	}

	private void updateScore(int bonus,int index) {
		combo++;
		if(combo>20)combo=20;
		score+=combo*bonus;
		comboList.add(new PapiCombo(combo, bonus, npcs.get(index).getX(),npcs.get(index).getY()));
		
	}

	private long timeSinceSpwan(){
		
		return ArcadeMachine.getTime()-timeSpawned;
		
	}

	private long timeElapsed(){
		return ArcadeMachine.getTime()-timeStart;
	}

	private void spawnEnemy(){
		spernTime-=(spernTime-700)*.5;
		int choices=(int)(timeElapsed()/timeTilCarrots)+1;
		if(choices>4)choices=4;
		int spawn= (int)(Math.random()*choices);

		npcs.add(new PapiEnemy(spawn));
		//npcs.get(npcs.size()-1).setDX(.1);
		timeSpawned=ArcadeMachine.getTime();;

	}


}
