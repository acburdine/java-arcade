package com.acburdine.papitramp;

import com.acburdine.arcademachine.ArcadeMachine;

public class PapiCombo {
	private int score, combo;
	public int dy=0;
	private double xCor,yCor;
	private long timeSpawned,timeOfDeath,timeAlive=1000;

	
	public PapiCombo(int score, int combo, double xCor,double yCor){
		this.score=score;
		this.combo=combo;
		this.xCor=xCor;
		this.yCor=yCor;
		timeSpawned=ArcadeMachine.getTime();
		timeOfDeath=timeSpawned+timeAlive;
	}
	
	public int getScore(){
		return score;
	}
	public int getCombo(){
		return combo;
	}
	public double getX(){
		return xCor;
	}
	public double getY(){
		return yCor;
	}
	public long getTimeOfDeath(){
		return timeOfDeath;
	}
	public long getTimeAlive(){
		return (long) (ArcadeMachine.getTime()-timeSpawned+.0001);
	}
	
	public long getLifeDuration(){
		return timeOfDeath;
	}
}
