package com.acburdine.robotworld;

	

import info.gridworld.grid.Location;



public class ChaseBot extends Defensive {
	
			/**
			 * Constructs a ChaseBot with a visionType of midVision.
			 */
			public ChaseBot() 
			{
				setVisionType(midVision);
				act();
			}
			
			
		    /**
		     * Turns the robot 90 degrees to the right without changing its location.
		     */
			 public void turn()
			    {
			        setDirection(getDirection() + Location.HALF_CIRCLE);
			    }
		    
			 /**
		     * Moves the robot into next space if it is valid. Otherwise, turns the robot 90 degrees.
		     */
		    public void act()
	    	{
	    		if(canMove())
	    			move();
	    		else
	    			turn();
	    	}
}
