package com.acburdine.robotworld;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import com.acburdine.arcademachine.ArcadeMachine;
import com.acburdine.arcademachine.MasterMenu;
import com.acburdine.arcademachine.Screen;
import com.acburdine.arcademachine.entities.Button;

/**
 * Menu for starting the RobotWorld Game
 * @author Austin Burdine, Ariel Sandberg
 *
 */
public class RobotWorldMenu implements Screen {
	
	ArrayList<Button> buttons;
	Button b1;
	Button b2;
	Button b3;
	
	/**
	 * Initializes buttons
	 */
	public RobotWorldMenu() {
		buttons = new ArrayList<Button>();
		b1 = new Button(ArcadeMachine.WIDTH/2 - 100, ArcadeMachine.HEIGHT/10, 200, 30, 24,
				"minecraftia", "Standard Bot", Color.darkGray, Color.black);
		b2 = new Button(ArcadeMachine.WIDTH/2 - 100, ArcadeMachine.HEIGHT/10*5, 200, 30, 24,
				"minecraftia", "Tank Bot", Color.darkGray, Color.black);
		b3 = new Button(ArcadeMachine.WIDTH/2 - 100, ArcadeMachine.HEIGHT/10*9, 200, 30, 24,
				"minecraftia", "Fast Bot", Color.darkGray, Color.black);
		buttons.add(b1);
		buttons.add(b2);
		buttons.add(b3);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Renders the screen
	 */
	public void render() {
		glClear(GL_COLOR_BUFFER_BIT);
		Color.red.bind();
		glRectd(0,0,ArcadeMachine.WIDTH, ArcadeMachine.HEIGHT);
		
		for (Button b : buttons) {
			b.update();
			b.draw();
			if(b.isHovering()) {
				if(b.hasBeenClicked()) {
					if(b.equals(b1)) {
						NormalBot norm = new NormalBot();
						new RobotWorld(norm);
					}
					if(b.equals(b2)) {
						TankBot tank = new TankBot();
						new RobotWorld(tank);
					}
					if(b.equals(b3)) {
						FastBot fast = new FastBot();
						new RobotWorld(fast);
					}
				}
			}
		}
	}

	/**
	 * Checks keyboard input
	 */
	public void input() {
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
			ArcadeMachine.setState(new RobotWorld());
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
			ArcadeMachine.setState(new MasterMenu());
		}
	}

}