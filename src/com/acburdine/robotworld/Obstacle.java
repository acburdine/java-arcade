package com.acburdine.robotworld;
import java.awt.Color;

public class Obstacle extends FieldElement {
	
	/**
     * Constructs a default Wall object.
     */
	public Obstacle() 
	{
		super();
	}
	
	/**
     * Constructs a Wall of specified color.
     * @param color - color of obstacle
     */
	public Obstacle(Color color)
	{
	    super(color);
	}
}
