package com.acburdine.robotworld;
	import info.gridworld.actor.*;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

	public class GoalieBot extends Defensive {
		
		 /**
	     * Constructs a default GoalieBot with a vision type of short vision.
	     */
		public GoalieBot() 
		{
			setVisionType(shortVision);
			act();
		}
		
		 /**
	     * Turns the robot 90 degrees to the right without changing its location.
	     */
	    public void turn()
	    {
	        setDirection(getDirection() + Location.HALF_CIRCLE);
	    }
	    
	    /**
	     * Moves the robot into next space if it is valid. Otherwise, turns the robot 90 degrees.
	     */
	    public void act()
	    {
	        if (canMove())
	            move();
	        else
	            turn();
	    }
	    
		/**
	     * Tests whether this robot can move forward into the next location. Valid spots contain nothing, an
	     * Obstacle or an Offensive Bot.
	     * @return true if this robot can move.
	     */
	    public boolean canMove()
	    {
	        Grid<Actor> gr = getGrid();
	        if (gr == null)
	            return false;
	        Location loc = getLocation();
	        Location next = loc.getAdjacentLocation(detect());
	        if (!gr.isValid(next))
	            return false;
	        Actor neighbor = gr.get(next);
	        return (neighbor == null) || (neighbor instanceof Offensive) || (neighbor instanceof Obstacle);
	    }
		
	}
