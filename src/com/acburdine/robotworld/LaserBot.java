package com.acburdine.robotworld;

	import info.gridworld.actor.*;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.Color;
public class LaserBot extends Defensive {	
	public static boolean missileCreated = false;	

		/**
		 * Constructs a LaserBot with a visionType of full Vision.
		 */
		public LaserBot() 
		{
			setVisionType(fullVision);
		}
		 
		/**
		 * LaserBot scans the field for Offensive bots. If Offensive bot is detected, shoots a Fire actor in the direction of that bot.
		 */
		   public void act()
	    	{
		    if(shouldShoot()){
		    	shoot();
		    	missileCreated = true;
	    		}
	    	}
	    
		    /**
		     * Shoots a Fire actor at a detected Offensive bot. 
		     */
		    public void shoot() {
		    	int direction = detect();
		    	setDirection(direction);
		    	Fire actor = new Fire();
		    	Location loc = getLocation().getAdjacentLocation(direction);
		    	if(getGrid().get(loc) instanceof Offensive)
		    		((Offensive) getGrid().get(loc)).respawn();
		    	actor.putSelfInGrid(getGrid(), loc);
		    	actor.setDirection(direction);
		 	  }
		    
		    /**
		     * Determines if LaserBot should shoot. Shoots if a Fire actor is not already in the grid and if it detects an Offensive bot.
		     * @return - true if LaserBot should shoot
		     */
		    public boolean shouldShoot(){
		    	if(missileCreated == false && detect() != getDirection()) 
		    		return true;
		    	return false;
		    }
}
