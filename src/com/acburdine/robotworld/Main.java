	package com.acburdine.robotworld;
import info.gridworld.actor.*;
import info.gridworld.grid.*;

public class Main {

	public static final int gridlength = 10;
	public static final int gridwidth = 10;
	public static void main(String[] args) {
		ActorWorld world = new ActorWorld();
		for(int i = 0; i < gridlength; i++) {
			for(int j = 0; j<gridwidth; j++) {
				Wall wall = new Wall();
				world.add(wall);
				Location loc = new Location(0,i);
				wall.moveTo(loc);
			}
		}
		
		Fire fire = new Fire();
		
		NormalBot norm = new NormalBot();
		GoalieBot bot = new GoalieBot();
//		PowerUp power = new PowerUp();
		world.add(bot);

//		world.add(power);
		world.show();
		
	}

}
