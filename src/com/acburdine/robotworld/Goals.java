package com.acburdine.robotworld;
import java.awt.Color;

public class Goals extends FieldElement{
	 
	private static final Color DEFAULT_COLOR = Color.YELLOW;
	 	/**
	     * Constructs a Goal with default color yellow.
	     */
		public Goals() {
			setColor(DEFAULT_COLOR);
		}
		
		 /**
	     * Constructs a Goal with specified color.
	     * @param color - the color of goal.
	     */
		 public Goals(Color color)
		    {
		        setColor(color);
		    }
		
}

