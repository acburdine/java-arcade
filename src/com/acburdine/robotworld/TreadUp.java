package com.acburdine.robotworld;
import java.awt.Color;


public class TreadUp extends FieldElement{
	 private static final Color DEFAULT_COLOR = Color.GREEN;
	 	/**
	     * Constructs a default TreadUp object.
	     */
		public TreadUp() {
			setColor(DEFAULT_COLOR);
		}
		
		 /**
	     * Constructs a TreadUp with specified color.
	     * @param color - the color of TreadUp.
	     */
		 public TreadUp(Color color)
		 {
		    setColor(color);
		 }
}
