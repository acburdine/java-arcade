package com.acburdine.robotworld;
import java.awt.Color;

import info.gridworld.actor.*;
import info.gridworld.grid.*;

public class Wall extends FieldElement{	    
		
		/**
	     * Constructs a default Wall object.
	     */
		public Wall() 
		{
			super();
		}
		
		/**
	     * Constructs a Wall of specified color.
	     * @param color - color of obstacle
	     */
		public Wall(Color color)
		{
		    super(color);
		}
		
	
	}


