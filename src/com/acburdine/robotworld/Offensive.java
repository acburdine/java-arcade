package com.acburdine.robotworld;
import info.gridworld.actor.*;
import info.gridworld.grid.*;
import info.gridworld.gui.WorldFrame;

import java.awt.Color;

public abstract class Offensive extends Actor{
	private int speed;
	private int wheelType;
	private boolean moved = true;
	private static final Color DEFAULT_COLOR = Color.BLUE;
	private Location start = new Location (1,1);
	public static final int slow = 1;
	public static final int fast = 2;
	public static final int tank = 0;
	public static final int normal = 1;
    
	/**
	 * Constructs a Offensive bot with a default color of blue.
	 */
	public Offensive() 
	{
		setColor(DEFAULT_COLOR);
	}
	
	/**
	 * Returns the speed of a given Offensive bot.
	 * @return speed - speed of given Offensive bot. 
	 */
	public int getSpeed() {
	return speed;
	}
	
	/**
	 * Returns the wheelType of a given Offensive bot.
	 * @return wheelType - wheelType of given Offensive bot. 
	 */
	public int getWheelType() {
		return wheelType;
	}
	
	/**
	 * Sets the speed of a given Offensive bot. 
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
		
	/**
	 * Sets the wheelType of a given Offensive bot. 
	 */
	public void setWheelType(int wheelType) {
		this.wheelType = wheelType;
	}
	
	/**
     * Moves the robot in direction input by user. A robot with a speed of slow will move forward one space at a time, a robot with a speed of fast will 
     * move forward two spaces at a time.
     */
    public void move()
    {
    	if(moved)
    		return; 
    	moved = true;
    	if(getSpeed() == slow) {
    		int locRow = getLocation().getAdjacentLocation(getDirection()).getRow();
    		int locCol = getLocation().getAdjacentLocation(getDirection()).getCol();
    		Location loc = new Location(locRow, locCol);
    		moveTo(loc);
    	}
    	if(getSpeed() == fast) {
    		int locRow = getLocation().getAdjacentLocation(getDirection()).getRow();
    		int locCol = getLocation().getAdjacentLocation(getDirection()).getCol();
    		Location loc = new Location(locRow, locCol);
    		int twoAwayRow = loc.getAdjacentLocation(getDirection()).getRow();
    		int twoAwayCol = loc.getAdjacentLocation(getDirection()).getCol();
    		Location loc2 = new Location(twoAwayRow, twoAwayCol);
    		moveTo(loc2);
    	}
    }

    /**
     * Tests whether this robot can move forward into the next location. 
     * Valid spaces include ones that contain nothing or Field. If the robot moves into a space that 
     * contains a PowerUp or a TreadUp, its respective instance variable changes accordingly.
     * If it moves into a space next to a goal, the robot wins the game.
     * Elements.
     * @return true if this robot can move.
     */
    public boolean canMove()
    {
    	Grid<Actor> gr = getGrid();
        if (gr == null)
            return false;
        Location loc = getLocation();
        Location next = loc.getAdjacentLocation(getDirection());
        Location twoAway = next.getAdjacentLocation(getDirection());
        Location wanted = null;
        if(getSpeed() == slow) 
        	wanted = next;
        if(getSpeed() == fast) 
        	wanted = twoAway;
        if (!gr.isValid(wanted))
        		return false;
        Actor neighbor = gr.get(wanted);
        if(neighbor instanceof PowerUp)
        	setSpeed(fast);
        if(neighbor instanceof TreadUp)
        	setWheelType(tank);
        if(getWheelType() == tank) {
        	if(neighbor instanceof Goals) {
            	return win();
            }
        	return (neighbor == null) || (neighbor instanceof FieldElement);
        }
        else {
        	if(neighbor instanceof Goals) {
            	return win();
            } else if(getSpeed() == fast) {
            	for(Location l:gr.getOccupiedAdjacentLocations(loc)) {
            		if(gr.get(l) instanceof Goals) {
            			return win();
            		}
            	}
            }
        	return (neighbor == null);
        }
        }
    
    /**
     * Takes in user input from arrow keys.
     * @return true if robot has moved.
     */
  	public boolean callInput() {
  		java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager()
  		.addKeyEventDispatcher(new java.awt.KeyEventDispatcher() { 
  		public boolean dispatchKeyEvent(java.awt.event.KeyEvent event) { 
  	    	boolean move = false;
  			String key = javax.swing.KeyStroke.getKeyStrokeForEvent(event).toString(); 
  			if (key.equals("pressed UP"))  {
  				setDirection(0); 
  				moved = false;
  			}
  			if (key.equals("pressed RIGHT")) {
  				setDirection(90); 
  				moved = false;
  			}
  			if (key.equals("pressed DOWN")) {
  				setDirection(180); 
  				moved = false;
  			}
  			if (key.equals("pressed LEFT")) {
  				setDirection(270);
  				moved = false;
  			}
  			return moved;
  		}
  	});
  		return true;
  	}
  	
    /**
     * Displays pop-up "win" screen.
     * @return false always
     */
  	private boolean win() {
  		WorldFrame.winDisplay();
  		return false;
  	}
  	
    /**
     * Checks the start location in which the offensive bot is respawned to see if it is surrounded
     * by a defensive bot. If it is, the defensive bot is removed from the grid.
     */
  	private void checkStart() {
  		if(getGrid().get(start) != null && getGrid().get(start) instanceof Defensive)
  			getGrid().get(start).removeSelfFromGrid();
  		for(Location l: getGrid().getOccupiedAdjacentLocations(start)) {
  			if(getGrid().get(l) instanceof Defensive) {
  				getGrid().get(l).removeSelfFromGrid();
  			}
  		}
  	}
  	
    /**
     * Respawns the offensive robot in the "start" location.
     */
  	public void respawn() {
  		RobotWorld.respawn().checkStart();
  	}
}
	
