package com.acburdine.robotworld;
import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.Color;

public class FastBot extends Offensive {
	 
	/**
     * Constructs a default Fastbot with a speed of fast and a wheelType of normal.
     */
	public FastBot() 
	{
		setSpeed(fast); 
		setWheelType(normal);
		act();
	}

	 /**
     * Reads in user input and moves in direction indicated by user if that space is valid.
     */
	public void act()
	{
		callInput();
		if(canMove())	
			move();
	}
}
