package com.acburdine.robotworld;
import info.gridworld.actor.*;
import info.gridworld.grid.*;

import java.awt.Color;

public abstract class Defensive extends Actor{
	private int visionType;
	private static final Color DEFAULT_COLOR = Color.RED;
	public static final int fullVision = 10;
	public static final int midVision = 2;
	public static final int shortVision = 0;
    
	/**
	 * Constructs a Defensive Bot with a default color of red.
	 */
	public Defensive() 
	{
		super();
		setColor(DEFAULT_COLOR);
	}
	
	/**
	 * Returns the visionType of a given Defensive bot.
	 * @return visionType - visionType of given Defensive bot. 
	 */
	public int getVisionType() {
		return visionType;
	}
	
	/**
	 * Sets the vision type of a given Defensive bot. 
	 */
	public void setVisionType(int visionType) {
			this.visionType = visionType;
	}

    /**
     * Moves the robot forward in direction of detected Offensive Bot.
     */
    public void move()
    {
        Grid<Actor> gr = getGrid();
        if (gr == null)
            return;
        Location loc = getLocation();
        Location next = loc.getAdjacentLocation(detect());
        if (gr.isValid(next)) {
        	Actor a = gr.get(next);
        	moveTo(next);
        	if(a instanceof Offensive)
            	((Offensive) a).respawn();
        }
        else
            removeSelfFromGrid();
    }
	/**
     * Tests whether this robot can move forward into the next location. Valid spots contain nothing or 
     * an Offensive Bot
     * @return true if this robot can move.
     */
    public boolean canMove()
    {
        Grid<Actor> gr = getGrid();
        if(gr == null)
            return false;
        Location loc = getLocation();
        Location next = loc.getAdjacentLocation(detect());
        if (!gr.isValid(next))
            return false;
        Actor neighbor = gr.get(next);
        return (neighbor == null) || (neighbor instanceof Offensive);
    }
	
	   /**
	    * Checks grid to determine location of Offensive bot relative to itself. Moves one space in that direction. If no Offensive bot is detected, 
	    * returns Defensive's bot current direction.
	    * @return dir - direction of detected Offensive bot or of Defensive bot's current direction.
	    */
	public int detect() {
	int dir = 0;
	for(int i = getLocation().getRow()-getVisionType(); i < getLocation().getRow()+getVisionType(); i++){
		for (int j =getLocation().getCol()-getVisionType(); j<getLocation().getCol()+getVisionType(); j++) {
			Location loc = new Location (i,j);
			if(getGrid().isValid(loc) && getGrid().get(loc) instanceof Offensive) {
				dir = getLocation().getDirectionToward(loc);
				return dir;
			}
		}
	}
	return getDirection();
	}
}
