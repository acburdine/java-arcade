package com.acburdine.robotworld;
import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.Color;


public class NormalBot extends Offensive {
	 /**
     * Constructs a default NormalBot with a speed of slow and a wheelType of normal.
     */
	public NormalBot() 
	{
		setSpeed(slow); 
		setWheelType(normal);
	}

	 /**
     * Reads in user input and moves in direction indicated by user if that space is valid.
     */
	public void act()
    	{
		callInput();
		if(canMove())	
			move();
    	}
}
