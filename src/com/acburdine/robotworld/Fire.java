package com.acburdine.robotworld;
import java.awt.Color;
import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

public class Fire extends Defensive{

	 /**
     * Constructs a Fire object with a default color of red.
     */
	public Fire() 
	{
		setColor(Color.RED);
		act();
	}

	 /**
     * Moves forward one space at a time until it contacts a Field Element, in which case it removes itself from the grid.
     */
    public void act()
    {
        Grid<Actor> gr = getGrid();
        if (gr == null)
            return;
        Location loc = getLocation();
        Location next = loc.getAdjacentLocation(getDirection());
        if (gr.isValid(next) && !(gr.get(next) instanceof FieldElement) && !(gr.get(next) instanceof Defensive)) {
        	Actor a = gr.get(next);
        	moveTo(next);
        	if(a instanceof Offensive)
            	((Offensive) a).respawn();
            
        }
        else {
            removeSelfFromGrid();
        	LaserBot.missileCreated = false;
        }
    }
}

