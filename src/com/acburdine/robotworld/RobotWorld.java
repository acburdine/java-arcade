package com.acburdine.robotworld;

import com.acburdine.arcademachine.GridWorldGame;
import info.gridworld.actor.Actor;
import info.gridworld.grid.Location;

public class RobotWorld extends GridWorldGame {
	
	private static RobotWorld runner;
		
	 /**
     * Creates a default RobotWorld with a Normal Bot as the offensive player.
     */
	public RobotWorld() {
		this(new NormalBot());
	}

	 /**
     * Creates a RobotWorld with an Offensive Bot selected by user (in-put retrieved from RobotWorldMenu).
     */
	public RobotWorld(Offensive o) {
		placeOffensiveBot(o);
		super.showWorld();
		runner = this;
	}
	
	 /**
     * Creates a bounded grid of dimensions 19 by 19 and calls the methods which place the Field Elements,
     * Defensive Bots and Offensive Bots in the grid in the appropriate locations.
     * .
     */
	@Override
	protected void setUpEntites() {
		super.initDimensions(19, 19);
		placeWalls();
		placeObstacles();
		placeGoals();
		placeChaseBots();
		placeLaserBots();
		placeGoalieBots();
		placePowerUp();
		placeTreadUp();
	}
	
	 /**
     * Places Walls along the border of the field (grid).
     * .
     */
	public void placeWalls() { 
		for(int i = 0; i < gridLength; i++) {
			Wall wall = new Wall();
			Wall wall2 = new Wall();
			Wall wall3 = new Wall();
			Wall wall4 = new Wall();
			Location loc = new Location(0,i);
			Location loc2 = new Location(gridWidth-1, i);
			Location loc3 = new Location(i, 0);
			Location loc4 = new Location(i,gridWidth-1);
			wall.putSelfInGrid(gr, loc);
			wall.moveTo(loc);
			wall2.putSelfInGrid(gr, loc2);
			wall2.moveTo(loc2);
			wall3.putSelfInGrid(gr, loc3);
			wall3.moveTo(loc3);
			wall4.putSelfInGrid(gr, loc4);
			wall4.moveTo(loc4);
		}
	}
	
	 /**
     * Randomly places obstacles throughout the field.
     * .
     */
	public void placeObstacles() { 
		for (int i = 0; i < 40; i++) {
	   	   Obstacle obs = new Obstacle();
	   	   int random1 = (int) (Math.random()*gridWidth);
	   	   int random2 = (int) (Math.random()*gridWidth);
	   	   Location location = new Location(random1,random2);
	   	   obs.putSelfInGrid(gr, location);
	   	   obs.moveTo(location);
		 }
	}
	
	 /**
     * Places 3 Goals along the left-hand border of the field.
     * .
     */
	public void placeGoals(){
		for (int i = 0; i < 3; i ++) {
			Goals goal = new Goals();
			Location loc = new Location(gridLength/2-1+i, gridLength-1);
			goal.putSelfInGrid(gr, loc);
			goal.moveTo(loc);
		}
	}	
	
	 /**
     * Randomly places ChaseBots throughout the field. Bots cannot be placed in a location already
     * occupied by a Field Element or Offensive Bot. 
     */
	public void placeChaseBots(){
		for (int i = 0; i < 5; i++) {
			ChaseBot chase = new ChaseBot();
			Location loc = null;
			while (loc == null) {
				int random1 = (int) (Math.random()*gridWidth);
		   	   	int random2 = (int) (Math.random()*gridWidth);
		   	   	Location temp = new Location (random1, random2);
		   	   	Actor neighbor = gr.get(temp);
		   	   	if(!(neighbor instanceof FieldElement) && !(neighbor instanceof Offensive))
		   	   		loc = new Location(random1,random2);
			}
			chase.putSelfInGrid(gr, loc);
			chase.moveTo(loc);
		}
	}
	
	 /**
     * Randomly places LaserBots throughout the field. Bots cannot be placed in a location already
     * occupied by a Field Element or Offensive Bot. 
     */
	public void placeLaserBots(){
		for (int i = 0; i < 3; i++) {
		   	   LaserBot laser = new LaserBot();
		   	   Location loc = null;
		   	   		while (loc == null) {
		   	   			int random1 = (int) (Math.random()*gridWidth);
		   	   			int random2 = (int) (Math.random()*gridWidth);
		   	   			Location temp = new Location (random1, random2);
		   	   			Actor neighbor = gr.get(temp);
		   	   			if(!(neighbor instanceof FieldElement) && !(neighbor instanceof Offensive))
		   	   				loc = new Location(random1,random2);
		   	   		}	
		   	 laser.putSelfInGrid(gr, loc);
		   	 laser.moveTo(loc);
			 }
	}
	
	 /**
     * Places one GoalieBot in the column directly in front of the Goals.
     */
	public void placeGoalieBots(){
		   GoalieBot goalie = new GoalieBot();
		   Location location = new Location(gridWidth-2, gridWidth-2);
		   goalie.putSelfInGrid(gr, location);
	}
	
	 /**
     * Randomly places a PowerUp in the field. PowerUp cannot be placed in a location already
     * occupied by a Field Element, Offensive Bot or Defensive Bot. 
     */
	public void placePowerUp() {
		for (int i = 0; i < 1; i++) {
		   	   PowerUp power = new PowerUp();
		   	   Location loc = null;
		   	   		while (loc == null) {
		   	   			int random1 = (int) (Math.random()*gridWidth);
		   	   			int random2 = (int) (Math.random()*gridWidth);
		   	   			Location temp = new Location (random1, random2);
		   	   			Actor neighbor = gr.get(temp);
		   	   			if(!(neighbor instanceof FieldElement) && !(neighbor instanceof Offensive) && !(neighbor instanceof Defensive))
		   	   				loc = new Location(random1,random2);
		   	   		}	
		   	 power.putSelfInGrid(gr, loc);
		   	 power.moveTo(loc);
			 }
	}
	
	 /**
     * Randomly places a TreadUp in the field. TreadUp cannot be placed in a location already
     * occupied by a Field Element, Offensive Bot or Defensive Bot. 
     */
	public void placeTreadUp() {
		for (int i = 0; i < 1; i++) {
		   	   TreadUp tread = new TreadUp();
		   	   Location loc = null;
		   	   		while (loc == null) {
		   	   			int random1 = (int) (Math.random()*gridWidth);
		   	   			int random2 = (int) (Math.random()*gridWidth);
		   	   			Location temp = new Location (random1, random2);
		   	   			Actor neighbor = gr.get(temp);
		   	   			if(!(neighbor instanceof FieldElement) && !(neighbor instanceof Offensive) && !(neighbor instanceof Defensive))
		   	   				loc = new Location(random1,random2);
		   	   		}	
		   	 tread.putSelfInGrid(gr, loc);
		   	 tread.moveTo(loc);
			 }
	}
	
	 /**
     * Places an Offensive Bot in the "start" position of the field.
     */
	public void placeOffensiveBot(Offensive o) {
		o.putSelfInGrid(gr, new Location(1,1));
	}
	
	 /**
     * Respawns the Offensive Bot as a Normal Bot. Calls a method that places the Bot in 
     * the "start" location.
     */
	public static Offensive respawn() {
		NormalBot n = new NormalBot();
		runner.placeOffensiveBot(n);
		return n;
	}

}
