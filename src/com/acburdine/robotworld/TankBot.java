package com.acburdine.robotworld;
import info.gridworld.actor.*;
import info.gridworld.grid.*;

import java.awt.Color;

public class TankBot extends Offensive {

	/**
     * Constructs a default Tankbot with speed of slow and wheelType of tank.
     */
	public TankBot() 
	{
		setSpeed(slow); 
		setWheelType(tank);
	}
	 
	 /**
     * Reads in user input and moves in direction indicated by user if that space is valid.
     */
	public void act()
    	{
		callInput();
		if(canMove())	
			move();
    	}
}
