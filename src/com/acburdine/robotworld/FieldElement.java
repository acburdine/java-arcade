package com.acburdine.robotworld;
import info.gridworld.actor.*;
import info.gridworld.grid.*;
import java.awt.Color;


public abstract class FieldElement extends Rock{
	 private static final Color DEFAULT_COLOR = Color.DARK_GRAY;
	 	/**
	     * Constructs a default FieldElement with default color of dark gray.
	     */
		public FieldElement() {
			setColor(DEFAULT_COLOR);
		}
		
		 /**
	     * Constructs a FieldElement with specified color.
	     * @param color - the color of FieldElement.
	     */
		 public FieldElement(Color color)
		    {
		        setColor(color);
		    }
		
}


