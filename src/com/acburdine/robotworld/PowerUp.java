package com.acburdine.robotworld;
import java.awt.Color;


public class PowerUp extends FieldElement {
	 private static final Color DEFAULT_COLOR = Color.YELLOW;
	 	/**
	     * Constructs  a PowerUp Object.
	     */
		public PowerUp() {
			setColor(DEFAULT_COLOR);
		}
		
		 /**
	     * Constructs a PowerUp object with specified color.
	     * @param color - the color of PowerUp.
	     */
		 public PowerUp(Color color)
		 {
		    setColor(color);
		 }
}
