package com.acburdine.pong;

import com.acburdine.arcademachine.entities.Ball;
import com.acburdine.arcademachine.entities.Paddle;

public class AIPaddle extends Paddle {
	
	Ball b;
	
	public AIPaddle(double x, double y, double width, double height, Ball ball){
		super(x,y,width,height);
		b = ball;
		
	}
}
