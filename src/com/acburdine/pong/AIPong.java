package com.acburdine.pong;

import org.lwjgl.input.Keyboard;

import com.acburdine.arcademachine.entities.Ball;
import com.acburdine.arcademachine.entities.Paddle;

public class AIPong extends PongGame {
	
	private int difficulty = 2;
	
	public void input() {
		if(Keyboard.isKeyDown(Keyboard.KEY_W)||Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			if((lp.getY()<=3))
				lp.setDY(0);
			else
				lp.setDY(-.2);
		}
		else if(Keyboard.isKeyDown(Keyboard.KEY_S)||Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			if((lp.getY()+lp.getHeight())>=HEIGHT-3)
				lp.setDY(0);
			else
				lp.setDY(.2);
		}
		else {
			lp.setDY(0);
		}
		double bY = b.getY()+b.getHeight()/2.0;
		double pY = rp.getY()+rp.getHeight()/2.0;
		if(rp.getY() < 5 || rp.getY()+rp.getHeight() > HEIGHT-5 || Math.abs(isBallAbove(bY,pY)) > rp.getHeight() / 4){
			double iba = isBallAbove(bY,pY);
			double moveSpeed;
			if(Math.abs(iba) < rp.getHeight()/2){
				moveSpeed = difficulty*.1*2*iba/rp.getHeight();
			} else {
				moveSpeed = difficulty*.1;
			}
				
				
			if(iba>0){
				rp.setDY(Math.abs(moveSpeed));
			} else {
				rp.setDY(-Math.abs(moveSpeed));
			
			}
		} else {
			rp.setDY(0);
		}
		if(rp.getY()<3){
			rp.setDY(0);
			rp.setY(5);
		}
		if(rp.getY()+rp.getHeight() > HEIGHT-3){
			rp.setDY(0);
			rp.setY(HEIGHT - 5 - rp.getHeight());
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)){
			if(endGame){
				getDelta();
				setUpEntites();
				b.setX(WIDTH/2);
				b.setY(HEIGHT/2);
				userScore = compScore = 0;
				endGame = false;
			}
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
			end();
		
		if(Keyboard.isKeyDown(Keyboard.KEY_I)&&Keyboard.isKeyDown(Keyboard.KEY_O)) {
			rp.setHeight(rp.getHeight()/1.05);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_K)&&Keyboard.isKeyDown(Keyboard.KEY_L)) {
			if(rp.getHeight()<5){
				rp.setHeight(5);
			}else{
				rp.setHeight(rp.getHeight()*1.05);
			}
		}
//		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
//			b.setDX(0);
//			b.setDY(0);
//			rp.setDX(0);
//			rp.setDY(0);
//		}
		
		
//		if(Keyboard.isKeyDown(Keyboard.KEY_UP)) {
//			if(rp.getY()<=20)
//				rp.setDY(0);
//			else
//				rp.setDY(-.2);
//		}
//		else if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
//			if((rp.getY()+rp.getHeight())>=HEIGHT-20)
//				rp.setDY(0);
//			else
//				rp.setDY(.2);
//		}
//		else {
//			rp.setDY(0);
//		}
	}
	
	protected void setUpEntites() {
		b = new Ball(WIDTH/2-10/2,HEIGHT/2-10/2,10,10);
		b.setDX(-.1);
		b.setDY(-.1);
		lp = new Paddle(10, HEIGHT/2-80/2,10,80);
		rp = new AIPaddle(WIDTH-20, HEIGHT/2-80/2,10,80,b);
		
	}
	
	private double isBallAbove(double ballY, double paddleY){
		return ballY - paddleY;
	}
	
	protected String getWinner(){
		if(player1Win){
			return "You win!";
		} else {
			return "You lose! loser";
		}
	}
}
