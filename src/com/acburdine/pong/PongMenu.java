package com.acburdine.pong;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import com.acburdine.arcademachine.ArcadeMachine;
import com.acburdine.arcademachine.MasterMenu;
import com.acburdine.arcademachine.Screen;
import com.acburdine.arcademachine.entities.Button;

public class PongMenu implements Screen {
	
	ArrayList<Button> buttons;
	Button b1;
	Button b2;
	
	
	public PongMenu() {
		buttons = new ArrayList<Button>();
		b1 = new Button(ArcadeMachine.WIDTH/2 - 100, ArcadeMachine.HEIGHT/10, 200, 30, 24,
				"minecraftia", "2 Player", Color.darkGray, Color.black);
		b2 = new Button(ArcadeMachine.WIDTH/2 - 150, ArcadeMachine.HEIGHT/10*9, 300, 30, 24,
				"minecraftia", "Versus Computer", Color.darkGray, Color.black);
		buttons.add(b1);
		buttons.add(b2);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	} 

	
	public void render() {
		glClear(GL_COLOR_BUFFER_BIT);
		Color.red.bind();
		glRectd(0,0,ArcadeMachine.WIDTH, ArcadeMachine.HEIGHT);
		
		for(Button button: buttons){
			button.update();
			
			if(button.isHovering()) {
				button.draw("Play", Color.lightGray);
				if(button.hasBeenClicked()) {
					if(button.equals(b1)) {
						ArcadeMachine.setState(new PongGame());
					}
					else if(button.equals(b2)) {
						ArcadeMachine.setState(new AIPong());
					}
				}
			}else{
			button.draw();
			}
		}
	}

	
	public void input() {
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
			ArcadeMachine.setState(new MasterMenu());
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
			ArcadeMachine.setState(new PongGame());
		}
	}

}
