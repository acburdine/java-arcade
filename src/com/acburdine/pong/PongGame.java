package com.acburdine.pong;

import java.util.Random;

import org.lwjgl.*;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.acburdine.arcademachine.ArcadeMachine;
import com.acburdine.arcademachine.Screen;
import com.acburdine.arcademachine.entities.Ball;
import com.acburdine.arcademachine.entities.Paddle;
import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.IFont;
import com.acburdine.arcademachine.utils.Sound;
import com.acburdine.arcademachine.utils.SoundLoader;

import static org.lwjgl.opengl.GL11.*;

/**
 * Plays the classic game Pong
 * @author Austin and Zach
 *
 */
public class PongGame implements Screen {
	
	public static final int WIDTH = 640;
	public static final int HEIGHT = 480;
	protected long lastFrame;
	protected Paddle lp;
	protected Paddle rp;
	protected Ball b;
	protected Sound hit;
	protected Sound miss;
	protected int userScore, win=4, leftWin=0, rightWin=0;
	protected int compScore;
	protected IFont font;
	protected TrueTypeFont userFont;
	protected boolean endGame=false, player1Win;
	
	/**
	 * called from ArcadeMachine to begin game
	 */
	public PongGame() {
		font = FontLoader.getFont("font_60");
		glEnable(GL_TEXTURE_2D);
		setUpTimer();
		setUpSound();
		userFont = font.getFont();
		setUpEntites();
	}
	/**
	 * loads hit and miss sound
	 */
	private void setUpSound() {
		hit = SoundLoader.getSound("hit");
		miss = SoundLoader.getSound("miss");
	}
	/**
	 * called every frame to read keyboard input
	 */
	public void input() {
		
		if(Keyboard.isKeyDown(Keyboard.KEY_I)&&Keyboard.isKeyDown(Keyboard.KEY_O)) {
			rp.setHeight(rp.getHeight()/1.05);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_K)&&Keyboard.isKeyDown(Keyboard.KEY_L)) {
			if(rp.getHeight()<5){
				rp.setHeight(5);
			}else{
				rp.setHeight(rp.getHeight()*1.05);
			}
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
			if((lp.getY()<=3))
				lp.setDY(0);
			else
				lp.setDY(-.2);
		}
		else if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
			if((lp.getY()+lp.getHeight())>=HEIGHT-3)
				lp.setDY(0);
			else
				lp.setDY(.2);
		}
		else {
			lp.setDY(0);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
			end();
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)){
			if(endGame){
				getDelta();
				setUpEntites();
				b.setX(WIDTH/2);
				b.setY(HEIGHT/2);
				userScore = compScore = 0;
				endGame = false;
			}
		}
			
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			b.setDX(0);
			b.setDY(0);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			if(rp.getY()<=3)
				rp.setDY(0);
			else
				rp.setDY(-.2);
		}
		else if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			if((rp.getY()+rp.getHeight())>=HEIGHT-3)
				rp.setDY(0);
			else
				rp.setDY(.2);
		}
		else {
			rp.setDY(0);
		}
	}
	/**
	 * ends the game and returns to the menu for Pong
	 */
	protected void end() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ArcadeMachine.setState(new PongMenu());
	}
	
	protected long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	protected int getDelta() {
		long currentTime = getTime();
		int delta = (int) (currentTime - lastFrame);
		lastFrame = getTime();
		return delta;
	}

	public void render() {
		if(endGame){
			winScreen();
			return;
		}
		
		logic(getDelta());
		glClear(GL_COLOR_BUFFER_BIT);
		glPushAttrib(GL_ENABLE_BIT); 
		short s = (short) 0xAAAA;
		glLineStipple(4, s);
		glEnable(GL_LINE_STIPPLE);
		glBegin(GL_LINES);
		glVertex3i(WIDTH/2, 0, 1);
		glVertex3i(WIDTH/2, HEIGHT, 1);
		glEnd();
		glPopAttrib();
		glBindTexture(GL_TEXTURE_2D, font.getTextureID());
		String userScore = this.userScore+"";
		String compScore = this.compScore+"";
		int userWidth = userFont.getWidth(userScore);
		int compWidth = userFont.getWidth(compScore);
		userFont.drawString(WIDTH/4-userWidth/2, 50, userScore, Color.white);
		userFont.drawString((WIDTH/4*3)-compWidth/2, 50, compScore, Color.white);
		glBindTexture(GL_TEXTURE_2D, 0);
		lp.draw();
		rp.draw();
		b.draw();
	}

	protected void winScreen() {
		glBindTexture(GL_TEXTURE_2D, 0);
		
		Color.magenta.bind();
		glRectd(0,0,WIDTH,HEIGHT);
		glBindTexture(GL_TEXTURE_2D, font.getTextureID());
		String winner = getWinner();

		centerString(50,winner, userFont, Color.green);
		centerString(200,"Enter to restart", userFont, Color.cyan);
		centerString(300,"" + leftWin + "  -  " + rightWin, userFont, Color.pink);
		
	}
	
	protected void centerString(double h, String s, TrueTypeFont ttf, Color c){
		int half= ttf.getWidth(s);
		ttf.drawString((WIDTH-half)/2, (float)h, s,c);
	}
	
	protected String getWinner(){
		if(player1Win){
			return "Player 1 Wins!";
		} else {
			return "Player 2 Wins!";
		}
	}

	protected void logic(int delta) {
		if(userScore>win||compScore>win){
			if(userScore>compScore){
				leftWin++;
				player1Win = true;
			} else {
				rightWin++;
				player1Win = false;
			}
			endGame=true;
			return;
		}
		b.update(delta);
		lp.update(delta);
		rp.update(delta);
		if(b.intersects(rp)) {
			b.setDX(-b.getDX()*1.3);
			double mid= (2*rp.getY()+rp.getHeight())/2;
			b.setDY(-Math.abs(b.getDY()));
			double dy=-.4*2*(mid-(b.getY()+b.getHeight()/2))/rp.getHeight();
			b.setDY(dy);
			hit.playSound();
		}
		if(b.intersects(lp)) {
			b.setDX(-b.getDX()*1.3);
			double mid= (2*lp.getY()+lp.getHeight())/2;
			b.setDY(-Math.abs(b.getDY()));
			double dy=-.4*2*(mid-(b.getY()+b.getHeight()/2))/lp.getHeight();
			b.setDY(dy);
			hit.playSound();
		}
//		if(b.intersects(lp)) {
//			b.setDX(0.3);
//			b.setDY(new Random().nextDouble() *.3);
//			hit.playSound();
//		} else if(b.intersects(rp)) {
//			b.setDX(-Math.abs(b.getDX()));
//			b.setDY(new Random().nextDouble() * .3);
//			hit.playSound();
//		}
		else if(b.getY()<=0) {
			b.setDY((Math.abs(b.getDY())));
			hit.playSound();
		}
		else if (b.getY()+b.getHeight()>=HEIGHT) {
			b.setDY(-Math.abs(b.getDY()));
			hit.playSound();
		}
		else if(isOutsideGrid(true)) {
			b.setLocation(WIDTH/2-10/2,HEIGHT/2-10/2);
			b.setDX(.1);
			b.setDY(new Random().nextDouble() *.3);
			miss.playSound();
			compScore++;
		} else if(isOutsideGrid(false)) {
			b.setLocation(WIDTH/2-10/2,HEIGHT/2-10/2);
			b.setDX(-.1);
			b.setDY(new Random().nextDouble() *.3);
			miss.playSound();
			userScore++;
		}
	}
	
	protected void setUpTimer() {
		lastFrame = getTime();
	}

	protected void setUpEntites() {
		lp = new Paddle(10, HEIGHT/2-80/2,10,80);
		rp = new Paddle(WIDTH-20, HEIGHT/2-80/2,10,80);
		b = new Ball(WIDTH/2-10/2,HEIGHT/2-10/2,10,10);
		b.setDX(-.1);
		b.setDY(-.1+Math.random()*.2);
		b.setX(WIDTH/2);
		b.setY(HEIGHT/2);
	}
	
	//true for left, false for right
	protected boolean isOutsideGrid(boolean side) {
		return (b.getX()+b.getWidth()>WIDTH && !side) || (b.getX()<0 && side);
	}

}
