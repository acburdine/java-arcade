package com.acburdine.snake;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.acburdine.arcademachine.ArcadeMachine;
import com.acburdine.arcademachine.Game;
import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.IFont;
import com.acburdine.arcademachine.utils.Sound;
import com.acburdine.arcademachine.utils.SoundLoader;

/**
 * Snake, the ever-twisting eating death game
 * 
 * @author Paul Sattizahn
 *
 */
public class Snake extends Game {
	
	protected Color wallColor= new Color(30,30,30), snakeColor=Color.blue;
	protected int score,x,y;
	protected Sound hit, miss, pause, death;
	protected ArrayList<Coordinate> snake;
	protected boolean[][] walls;
	protected Coordinate food;
	protected boolean win, dead;
	protected int direction;
	static final int UP=0,DOWN=1,LEFT=2,RIGHT=3;
	protected long timeLastMoved=ArcadeMachine.getTime(),timeForMove=150;
	protected long initialTFM=timeForMove+1;
	protected int stepsSinceLastMeal=10;
	protected boolean paused=false,lose=false;
	private IFont font;
	private TrueTypeFont ttf;
	protected double minTFM=20,difficulityIncrease=.052; //minimum time for move

	/**
	 * Constructs the Snake game
	 */
	public Snake() {
		font = FontLoader.getFont("font_30");
		glEnable(GL_TEXTURE_2D);
		init();
		ttf = font.getFont();
	}

	/**
	 * Sets up entities
	 */
	protected void setUpEntites() {
		snake = new ArrayList<Coordinate>();
		x=ArcadeMachine.WIDTH/20-2;
		y=ArcadeMachine.HEIGHT/20-3;
		walls=new boolean[y][x];
		for(int r=0; r<y; r++){
			for(int c=0; c<x; c++){
				if(r==0||r==y-1||c==0||c==x-1) walls[r][c]=true;
			}
		}
		snake.add(new Coordinate(x/2,y/2));
		snake.add(new Coordinate(x/2,y/2));
		snake.add(new Coordinate(x/2,y/2));
		snake.add(new Coordinate(x/2,y/2));
		snake.add(new Coordinate(x/2,y/2));
		snake.add(new Coordinate(x/2,y/2));
		snake.add(new Coordinate(x/2,y/2));
		direction= UP;
		food = new Coordinate(x/2, y/2);
		System.out.printf("x:%3d y:%3d, fx:%3d fy%3d", x,y, food.getX(), food.getY()  );
		
	}
	
	/**
	 * Gets the time difference
	 * @return - time difference
	 */
	protected long timeMoved(){
		return ArcadeMachine.getTime()- timeLastMoved;
	}

	/**
	 * Initializes score
	 */
	private void init() {
		score=0;
	}
	
	/**
	 * Ends the game and returns to the Snake menu
	 */
	private void end() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ArcadeMachine.setState(new SnakeMenu());
	}
	
	/**
	 * Sets up the sounds
	 */
	protected void setUpSound() {
		hit = SoundLoader.getSound("mariosounds_coin");
		miss = SoundLoader.getSound("mariosounds_door");
		pause = SoundLoader.getSound("mariosounds_Pause");
		death= SoundLoader.getSound("mariosounds_Death");
	}
	
	/**
	 * Renders the game
	 */
	public void render() {
		logic(ArcadeMachine.getDelta());
		Color.black.bind();
		glRectd(0,0,ArcadeMachine.WIDTH, ArcadeMachine.HEIGHT);
		wallColor.bind();
		for(int r=0; r<y; r++){
			for(int c=0; c<x; c++){
				if( walls[r][c]==true)
					glRectd(20+20*c,45+20*r,17+20+20*c,17+45+20*r);
			}
		}
		new Color(150,10,10).bind();
		glRectd(20+20*snake.get(0).getX(),45+20*snake.get(0).getY(),17+20+20*snake.get(0).getX(),17+45+20*snake.get(0).getY());
		snakeColor.bind();
		for(int i=1; i<snake.size();i++){
			int xCor=snake.get(i).getX();
			int yCor=snake.get(i).getY();
			glRectd(20+20*xCor,45+20*yCor,17+20+20*xCor,17+45+20*yCor);
		}
		
		Color.yellow.bind();
		glRectd(20+20*food.getX(),45+20*food.getY(),17+20+20*food.getX(),17+45+20*food.getY());
		glPushAttrib(GL_ENABLE_BIT);
		Color.white.bind(); //middle line color
		short s = (short) 0xAAAA;
		glLineStipple(4, s);
		glEnable(GL_LINE_STIPPLE);
		glBegin(GL_LINES);
			glVertex3i(0, 40, 1);
			glVertex3i(ArcadeMachine.WIDTH, 40, 1);
		glEnd();
		glPopAttrib();
		glBindTexture(GL_TEXTURE_2D, font.getTextureID());
		String score = this.score+"00";
		ttf.drawString(125, 5, "Score: "+score, Color.white);
		if(paused){
			ttf.drawString(150,100, "Paused. Press R to Continue", Color.white);
		}
		if(lose){
			ttf.drawString(150,100, "You lose...  (Enter to restart)", Color.white);
		}
		if(stepsSinceLastMeal<4+this.score/3)
			ttf.drawString(15+20*snake.get(0).getX(),45+20*snake.get(0).getY(), "Yummy!", Color.yellow);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	/**
	 * Game logic
	 */
	public void logic(int delta) {
		if(paused||lose) return;
		if(timeMoved()<timeForMove){
			return;
		}
		stepsSinceLastMeal++;
		Coordinate mew=getNextLocation();
		if(deathCheck(mew)){
			//insert death code here. I didn't want to make the game just close.
			lose=true;
			death.playSound();
		}
		if(mew.equals(food)){
			System.out.println("Yummy!");
			miss.playSound();
			stepsSinceLastMeal=0;
			score++;
			food= getEmpty();
			timeForMove-=(timeForMove-minTFM)*difficulityIncrease;
			System.out.print(timeForMove);
			snake.add(snake.get(snake.size()-1));
			snake.add(snake.get(snake.size()-1));
			snake.add(snake.get(snake.size()-1));
		}
		for(int i=snake.size()-1;i>0;i--){
			snake.set(i,snake.get(i-1));
		}
		snake.set(0,mew);
		timeLastMoved=ArcadeMachine.getTime();				
	}
	
	/**
	 * Gets the next location of the snake
	 * @return Coordinate - the next location
	 */
	protected Coordinate getNextLocation() {
		int x=snake.get(0).getX(),y=snake.get(0).getY();
		switch( direction){
		case UP: y--;
			break;
		case DOWN: y++;
			break;
		case LEFT: x--;
			break;
		case RIGHT: x++;
			break;
		default: 
		}
		if(x<1)x=this.x-2;
		if(y<1)y=this.y-2;
		if(x>this.x-2)x=1;
		if(y>this.y-2)y=1;
		return new Coordinate(x,y);
	}

	/**
	 * Checks if the next coordinate is one that the snake will die on
	 * @param mew - the next coordinate
	 * @return boolean - True if snake will die, otherwise false
	 */
	protected boolean deathCheck(Coordinate mew) {
		for(int i=0; i<snake.size();i++){
			if(mew.equals(snake.get(i))){
				System.out.println("I'm eating myself!");
				return true;
			}
		}
		return false;
	}

	/**
	 * Keyboard input method
	 */
	public void input() {
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)){
			if(lose==true){
				lose=false;
				resetSnake();
				death.stopSound();
			} 
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			if(!paused&&!lose){
				paused=true;
				pause.playSound();
			} 
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_R)){
			if(paused==true){
				paused=false;
				hit.playSound();
			} 
		}
	
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))end();
		boolean left= (Keyboard.isKeyDown(Keyboard.KEY_LEFT)||Keyboard.isKeyDown(Keyboard.KEY_A));
		boolean right= (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)||Keyboard.isKeyDown(Keyboard.KEY_D));
		boolean up= (Keyboard.isKeyDown(Keyboard.KEY_UP)||Keyboard.isKeyDown(Keyboard.KEY_W));
		boolean down= (Keyboard.isKeyDown(Keyboard.KEY_DOWN)||Keyboard.isKeyDown(Keyboard.KEY_S));
		
		if(left&&direction!=RIGHT){
			direction=LEFT;
		}else if(right&&direction!=LEFT){
			direction=RIGHT;	
		}else if(up&&direction!=DOWN){
			direction=UP;
		}else if(down&&direction!=UP){
			direction=DOWN;
		}
	}
	
	/**
	 * Resets the snake at the beginning
	 */
	private void resetSnake() {
		timeForMove=150;
		score=0;
		snake= new ArrayList<Coordinate>();
		setUpEntites();
		
	}
	
	/**
	 * @return
	 */
	protected Coordinate getEmpty(){
		while(true){
			int ex=(int)(Math.random()*(x-2));
			int why=(int)(Math.random()*(y-2));
			Coordinate c= new Coordinate(ex+1, why+1);
			boolean notOccupied=true;
			if(c.equals(food));
			for(int i=0; i< snake.size();i++){
				if(c.equals(snake.get(i))){
					notOccupied=false;
				}
			}
			if(notOccupied){
				return c;
			}
		}
	}

}
