package com.acburdine.snake;

import org.newdawn.slick.Color;

/**
 * Similar to Snake but has a border that will kill the snake it it hits it.
 * @author Paul Sattizahn
 *
 */
public class BoundedSnake extends Snake {

	/**
	 * Constructs the BoundedSnake game
	 */
	public BoundedSnake() {
		super();
		wallColor= new Color(150,150,150);
		snakeColor= Color.green;
	}
	
	/**
	 * Renders the walls in a light gray
	 */
	public void render() {
		Color.lightGray.bind();
		super.render();
	}
	
	/**
	 * Checks if the snake is dead
	 * @param new - coordinate the snake will hit
	 * @return boolean - True if snake is dead, otherwise false
	 */
	protected boolean deathCheck(Coordinate mew) {
		if(super.deathCheck(mew)) return true;
		if(mew.getX()<1||mew.getX()>=x-1||mew.getY()<1||mew.getY()>=y-1){
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the next Location the snake will hit
	 * @return Coordinate - next location the snake will go to
	 */
	protected Coordinate getNextLocation() {
		int x=snake.get(0).getX(),y=snake.get(0).getY();
		switch( direction){
		case UP: y--;
			break;
		case DOWN: y++;
			break;
		case LEFT: x--;
			break;
		case RIGHT: x++;
			break;
		default: 
		}
		return new Coordinate(x,y);
	}
}
