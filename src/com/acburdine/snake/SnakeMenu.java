package com.acburdine.snake;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.acburdine.arcademachine.ArcadeMachine;
import com.acburdine.arcademachine.MasterMenu;
import com.acburdine.arcademachine.Screen;
import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.IFont;

/**
 * Class for starting the Snake game
 * @author Paul Sattizahn
 *
 */
public class SnakeMenu implements Screen {

	private IFont font;
	private TrueTypeFont ttf;
	
	/**
	 * Loads the font
	 */
	public SnakeMenu() {
		font = FontLoader.getFont("font_30");
		ttf=font.getFont();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Renders the Screen
	 */
	public void render() {
		glBindTexture(GL_TEXTURE_2D, 0);
		glClear(GL_COLOR_BUFFER_BIT);
		Color.yellow.bind();
		glRectd(0,0,ArcadeMachine.WIDTH, ArcadeMachine.HEIGHT);
		
		glBindTexture(GL_TEXTURE_2D, font.getTextureID());
		ttf.drawString(150,100, "Enter for unbounded", Color.blue);
		ttf.drawString(150,300, "B for bounded", Color.red);	
	}
	
	/**
	 * Checks Keyboard input
	 */
	public void input() {
		if(Keyboard.isKeyDown(Keyboard.KEY_B)) {
			ArcadeMachine.setState(new BoundedSnake());
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
			ArcadeMachine.setState(new Snake());
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
			ArcadeMachine.setState(new MasterMenu());
		}
	}

}
