package com.acburdine.snake;

/**
 * Coordinate class for use with the Snake Method
 * @author Paul Sattizahn
 */
public class Coordinate {
	int x,y;
	
	/**
	 * Creates a coordinate with the specified x and y values
	 * @param ex - x value
	 * @param why - y value
	 */
	public Coordinate(int ex, int why) {
		x=ex;
		y=why;
	}
	/**
	 * Checks if one coordinate occupies the same x and y as another
	 * @param other - Another coordinate
	 * @return boolean - True if they overlap, otherwise false
	 */
	public boolean equals(Coordinate other){
		return this.getX()==other.getX()&&this.getY()==other.getY();
	}
	
	/**
	 * Gets the x coordinate
	 * @return int - the x coordinate
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * Sets the x coordinate
	 * @param ex - new x coordinate
	 */
	public void setX(int ex){
		x=ex;
	}
	
	/**
	 * Gets the y coordinate
	 * @return - the y coordinate
	 */
	public int getY(){
		return y;
	}
	
	/**
	 * Sets the y coordinate
	 * @param why - new y coordinate
	 */
	public void setY(int why){
		y=why;
	}
	
	/**
	 * Returns the string representation of the Coordinate
	 * @return String - the string representation of the Coordinate
	 */
	public String toString(){
		return "("+x+","+ y+ ")";
	}

}
