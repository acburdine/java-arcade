package com.acburdine.brickbreaker;

import static org.lwjgl.opengl.GL11.*;

import org.newdawn.slick.Color;

import com.acburdine.arcademachine.entities.AbstractEntity;
import com.acburdine.arcademachine.entities.Entity;

/**
 * Brick (similar to paddle, used in BrickBreaker)
 * @author Paul Sattizahn
 */
public class Brick extends AbstractEntity {
	boolean alive;
	Color color;

	/**
	 * Constructs a brick with the parameters
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - width of brick
	 * @param height - height of brick
	 */
	public Brick(double x, double y, double width, double height) {
		super(x, y, width, height);
		color= new Color((int)(Math.random()*255), (int)(Math.random()*255), (int)(Math.random()*255));
		alive=true;
	}

	/**
	 * Whether or not the brick has been destroyed
	 * @return boolean true if brick has not been destroyed; otherwise false
	 */
	public boolean isAlive(){
		return alive;
	}
	
	/**
	 * Destroys the brick
	 */
	public void kill(){
		alive=false;
	}
	
	/**
	 *  Draws the brick
	 */
	
	public void draw() {
		if(alive){
			color.bind();
			glRectd(x,y,x+width,y+height);
		}

	}

	/**
	 * @deprecated - unneccessary function
	 * @param delta - Change in system time since the last frame
	 */

	public void update(int delta) {

	}
	
	/**
	 * Whether or not another entity has hit the brick
	 * @param other - Other entity
	 * @return boolean - true if it has; otherwise false
	 */
	public boolean intersects(Entity other) {
		if(alive){
			hitbox.setBounds((int) x, (int) y, (int) width, (int) height);
			return hitbox.intersects(other.getX(), other.getY(), other.getWidth(), other.getHeight());
		}else return false;
	}
	
	/**
	 * Returns x,y position in coordinate format
	 * @return String - coordinate
	 */
	public String toString(){
		return "x: " + getX()+ " y: "+ getY();
	}
	
	public Color getColor(){
		return color;
	}

}
