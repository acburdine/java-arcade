package com.acburdine.brickbreaker;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.acburdine.arcademachine.ArcadeMachine;
import com.acburdine.arcademachine.Game;
import com.acburdine.arcademachine.MasterMenu;
import com.acburdine.arcademachine.entities.Ball;
import com.acburdine.arcademachine.entities.BallTextured;
import com.acburdine.arcademachine.entities.Ballv2;
import com.acburdine.arcademachine.entities.Paddle;
import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.IFont;
import com.acburdine.arcademachine.utils.Sound;
import com.acburdine.arcademachine.utils.SoundLoader;

/**
 * BrickBreaker Game
 * @author Paul Sattizahn
 * @version 1.0
 */
public class BrickBreaker extends Game {
	private Paddle paddle;
	private Ballv2 ball;
	private Sound hit;
	private Sound miss;
	private int score;
	private int lives;
	private Brick[] bricks;
	private IFont font;
	private TrueTypeFont userFont; 
	private boolean win,paused, starting=true;
	private double storedDX;
	private double storedDY;
	
	/**
	 * ArcadeMachine constructor, initiates a new BrickBreaker game
	 * 
	 */
	public BrickBreaker() {
		font = FontLoader.getFont("font_30");
		glEnable(GL_TEXTURE_2D);
		lives=3;
		score=0;
		userFont = font.getFont();
	}
	
	/**
	 * Sets up the hit and miss sounds
	 */
	protected void setUpSound() {
		hit = SoundLoader.getSound("hit");
		miss = SoundLoader.getSound("miss");
	}
	
	/**
	 * Called every frame. Checks keyboard input.
	 */
	public void input() {
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) end();
		if(Keyboard.isKeyDown(Keyboard.KEY_P)) ball.setDY(.25);
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			pause();
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_R)){
			resume();
		}
		boolean left=Keyboard.isKeyDown(Keyboard.KEY_LEFT);
		boolean right=Keyboard.isKeyDown(Keyboard.KEY_RIGHT);
		boolean down=Keyboard.isKeyDown(Keyboard.KEY_DOWN);
		
		if(down&&starting){
			starting=false;
			ball.setDX(Math.random()*.2-.1);
			ball.setDY(-.25);
		}
		
		if(left){
			if(paddle.getX()>5)paddle.setDX(-.5);
			else paddle.setDX(0);
		}
		
		else if(right){
			if(paddle.getX()<ArcadeMachine.WIDTH-paddle.getWidth()-5){
				paddle.setDX(.5);
			}else{
				paddle.setDX(0);
			}
		}
		else
			paddle.setDX(0);
	}

	/**
	 * Ends the game
	 */
	private void end() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ArcadeMachine.setState(new MasterMenu());
	}

	/**
	 * Called every frame. Renders the game
	 */
	public void render() {
		if(!win){
			logic(ArcadeMachine.getDelta());
			Color.black.bind();
			glRectd(0,0,ArcadeMachine.WIDTH, ArcadeMachine.HEIGHT);
			glPushAttrib(GL_ENABLE_BIT);
			Color.white.bind(); //middle line color
			short s = (short) 0xAAAA;
			glLineStipple(4, s);
			glEnable(GL_LINE_STIPPLE);
			glBegin(GL_LINES);
			glVertex3i(0, 40, 1);
			glVertex3i(ArcadeMachine.WIDTH, 40, 1);
			glEnd();
			glPopAttrib();
			glBindTexture(GL_TEXTURE_2D, font.getTextureID());
			String score = "" + this.score;
			String lives = this.lives+"";
			userFont.drawString(25, 5, "Score: "+score, Color.white);
			userFont.drawString(205, 5, "Lives: "+lives, Color.white);
			if(starting){
				userFont.drawString(ArcadeMachine.WIDTH/2-100, ArcadeMachine.HEIGHT/2, "Down to release", Color.white);
				
			}
			glBindTexture(GL_TEXTURE_2D, 0);
			for(int i=0; i<bricks.length;i++){
				bricks[i].draw();
			}
			Color.red.bind();//left paddle  color
			paddle.draw();
			Color.yellow.bind();//ball  color
			ball.draw();
			glBindTexture(GL_TEXTURE_2D, 0);
		}else {
			glBindTexture(GL_TEXTURE_2D, font.getTextureID());
			userFont.drawString(ArcadeMachine.WIDTH/2-100, ArcadeMachine.HEIGHT/2, "YOU WIN!", Color.white);
			glBindTexture(GL_TEXTURE_2D, 0);
		}
	}
	
	/**
	 * Lose screen
	 */
	private void lose(){//tried to make lose screen, but it doesn't work
		//Color.red.bind();
		//glRectd(0, 0, ArcadeMachine.WIDTH, ArcadeMachine.HEIGHT);
		//glBindTexture(GL_TEXTURE_2D, 1);
		//userFont.drawString(ArcadeMachine.WIDTH/2-100, ArcadeMachine.HEIGHT/2, "YOU LOSE!", Color.white);
		//	while((!Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))||(!Keyboard.isKeyDown(Keyboard.KEY_RETURN))){
		//}
		end();
	}
	
	/**
	 * Called every frame. Moves around the ball and paddle objects
	 */
	public void logic(int delta) {
		boolean winCheck=true;
		for(int i=0; i<bricks.length;i++){
			if(winCheck&&bricks[i].isAlive())winCheck=false;
			if(ball.intersects(bricks[i])){
				if(bricks[i].isAlive()){
					ball.setDY(-ball.getDY());
					bricks[i].kill();
					score += (int)(bricks[i].getColor().getBlue());
					hit.playSound();
				}
			}
		}
		if(winCheck)
			win=true;
		ball.update(delta);
		paddle.update(delta);
		
		if(starting){
			ball.setY(ArcadeMachine.HEIGHT-75);
			ball.setX(paddle.getX()+paddle.getWidth()/2);
		}
		if(lives == 0)
			lose();
		if(ball.intersects(paddle)&&ball.getY()<ArcadeMachine.HEIGHT-45) {
			double mid= (2*paddle.getX()+paddle.getWidth())/2;
			ball.setDY(-Math.abs(ball.getDY()));
			double dx=-.4*2*(mid-ball.getX())/paddle.getWidth();
			ball.setDX(dx);
			hit.playSound();
		}
		if(ball.getX()<0){
			ball.setDX(Math.abs(ball.getDX()));
			miss.playSound();
		}
		if(ball.getX()+ball.getWidth()>ArcadeMachine.WIDTH){
			ball.setDX(-Math.abs(ball.getDX()));
			miss.playSound();
		}
		if(ball.getY()<40){
			miss.playSound();
			ball.setDY(Math.abs(ball.getDY()));
		}
		if(ball.getY()>ArcadeMachine.HEIGHT){
			reset();
		}
	}
	
	/**
	 * Resets the ball position and dx value. Used upon loss of 1 life.
	 */
	private void reset(){
		starting=true;
		lives--;
	}

	/**
	 * Sets up the game entities
	 */
	protected void setUpEntites() {
		bricks=new Brick[60];
		for(int i=0; i<bricks.length;i++){
			bricks[i]= new Brick(20+60*(i%10),60+30*(i/10),50, 20);
		}
		paddle = new Paddle(ArcadeMachine.WIDTH/2-50,ArcadeMachine.HEIGHT-55,100,10);
		ball = new Ballv2(ArcadeMachine.WIDTH/2-5,200,5);
		ball.setDX(0);
		ball.setDY(0);
		ball.setY(ArcadeMachine.HEIGHT/2);
		
	}
	
	/**
	 * pauses the game and stores the velocity for restarting
	 * 
	 */
	private void pause(){
		if(paused)return;
		if(ball.getDX() != 0)
			storedDX = ball.getDX();
		if(ball.getDY() != 0)
			storedDY = ball.getDY();
		ball.setDX(0);
		ball.setDY(0);
		paused=true;
	}
	
	/**
	 * resumes the game
	 */
	private void resume(){
		if(!paused)return;
		ball.setDX(storedDX);
		ball.setDY(storedDY);
		paused=false;
	}
	
}
