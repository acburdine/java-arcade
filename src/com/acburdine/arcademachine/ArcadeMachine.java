package com.acburdine.arcademachine;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Mouse;
import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.GameLoader;
import com.acburdine.arcademachine.utils.SetUpMachine;
import com.acburdine.arcademachine.utils.SoundLoader;

/**
 * Main runner class for the ArcadeMachine program
 * @author Austin Burdine
 * @version 2.0
 *
 */
public class ArcadeMachine {
	
	public static final int WIDTH = 640;
	public static final int HEIGHT = 480;
	
	private Screen currentScreen;
	private long lastFrame;
	private boolean isRunning = true;
	private boolean isPaused = false;
	
	private static ArcadeMachine runner;
	
	/**
	 * Creates a default ArcadeMachine with the initial screen set to IntroScreen
	 */
	public ArcadeMachine() {
		this("IntroScreen");
	}
	
	/**
	 * Creates an ArcadeMachine with the initial screen set to s
	 * @param s - Initial Screen (NOTE: MUST BE THE CLASSNAME OF A CLASS THAT EXTENDS SCREEN!)
	 */
	public ArcadeMachine(String s) {
		init();
		setUpResources();
		setUpOpenGL();
		setUpTimer();
		runner = this;
		try {
			currentScreen = (Screen) Class.forName("com.acburdine.arcademachine."+s).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		run();
	}
	
	/**
	 * Runs the loop;
	 */
	private void run() {
		runLoop();
		if(isPaused)
			endWithoutExiting();
		else
			end();
	}
	
	/**
	 * The actual game running loop
	 */
	private void runLoop() {
		while(isRunning) {
			if(isPaused)
				break;
			render(currentScreen);
			Display.update();
			Display.sync(60);
			if(Display.isCloseRequested())
				isRunning = false;
		}
	}
	
	/**
	 * Loads the fonts and the sounds from the files
	 */
	private void setUpResources() {
		GameLoader.loadGames();
		FontLoader.loadFonts();
		SoundLoader.loadSounds();
	}

	/**
	 * Ends the machine and destroys it
	 */
	private void end() {
		SoundLoader.deleteSources();
		Display.destroy();
		AL.destroy();
		System.exit(0);
	}
	
	/**
	 * Ends without exiting the Java program
	 * @deprecated
	 */
	private void endWithoutExiting() {
		SoundLoader.deleteSources();
		Display.destroy();
		AL.destroy();
	}
	
	/**
	 * Sets up the timer
	 */
	private void setUpTimer() {
		lastFrame = getTime();
	}
		
	/**
	 * Sets the current screen to run
	 * @param s - New Screen
	 */
	public void setCurrentState(Screen s) {
		currentScreen = s;
	}
	
	/**
	 * Pauses the game
	 * @deprecated
	 */
	public void pause() {
		isPaused = true;
	}
	
	/**
	 * Resumes the game
	 * @deprecated
	 */
	public void resume() {
		isPaused = false;
	}
	
	/**
	 * Initializes the Display and OpenAL context
	 */
	private void init() {
		try {
			Display.setDisplayMode(new DisplayMode(WIDTH,HEIGHT));
			Display.setTitle("Arcade Machine");
			Display.setVSyncEnabled(true);
			Display.create();
			AL.create();
		} catch(LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets up OpenGL
	 */
	private void setUpOpenGL() {                
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0,WIDTH,HEIGHT,0,-1,1);
		glMatrixMode(GL_MODELVIEW);
	}
	
	/**
	 * Renders the current screen
	 * @param s - Screen to render
	 */
	private void render(Screen s) {
		s.render();
		s.input();
	}
	
	/**
	 * Ends the game
	 */
	public void endGame() {
		isRunning = false;
	}
	
	/**
	 * Checks if game is paused
	 * @return True if game is paused, otherwise false
	 * @deprecated
	 */
	public boolean isPaused() {
		return isPaused;
	}
	
	/**
	 * Gets the time difference from lastFrame to current system time
	 * @return int - Time difference
	 */
	public int getSystemTimeChange() {
		long currentTime = getTime();
		int delta = (int) (currentTime - lastFrame);
		lastFrame = getTime();
		return delta;
	}
	
	/**
	 * Gets the new system time
	 * @return long - The new system time in milliseconds
	 */
	public static long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	/**
	 * Ends the game. Part of the singleton pattern
	 */
	public static void kill() {
		runner.endGame();
	}
	
	/**
	 * Gets the system time change. Part of singleton pattern
	 * @return int - the time change
	 */
	public static int getDelta() {
		return runner.getSystemTimeChange();
	}
	
	/**
	 * Sets the current state of the ArcadeMachine game. Part of singleton pattern.
	 * @param s - New Screen
	 */
	public static void setState(Screen s) {
		runner.setCurrentState(s);
	}
	
	/**
	 * Reloads the ArcadeMachine. Doesn't work
	 * @deprecated
	 */
	public static void reload() {
		runner = new ArcadeMachine("MasterMenu");
	}
	
	/**
	 * Pauses the game
	 * @deprecated
	 */
	public static void pauseGame() {
		runner.pause();
	}
	
	/**
	 * Resumes the game
	 * @deprecated
	 */
	public static void resumeGame() {
		runner.resume();
	}
	
	/**
	 * Gets the x coordinate of the mouse point
	 * @return double - the x coordinate
	 */
	public static double getMouseX() {
		return Mouse.getX();
	}
	
	/**
	 * Gets the y coordinate of the mouse point
	 * @return double - the y coordinate
	 */
	public static double getMouseY() {
		return ArcadeMachine.HEIGHT - Mouse.getY();
	}
	
	/**
	 * Runs the running loop again. Doesn't work. Part of singleton pattern
	 * @deprecated
	 */
	public static void runLoopAgain() {
		runner.run();
	}
	
	/**
	 * Runs the arcade machine
	 * @param args - Java standard requirements for main method
	 */
	public static void main(String[] args) {
		if(SetUpMachine.isFirstRun()) {
			new SetUpMachine().runSetup();
		}
		new ArcadeMachine();
	}

}
