package com.acburdine.arcademachine;

import info.gridworld.actor.Actor;
import info.gridworld.actor.ActorWorld;
import info.gridworld.grid.BoundedGrid;
import info.gridworld.grid.Grid;

public abstract class GridWorldGame extends Game {

	protected Grid<Actor> gr;
	protected int gridLength;
	protected int gridWidth;
	protected ActorWorld world;
	
	//initialize gridLength & gridWidth
	protected void initDimensions(int width, int height) {
		gridLength = height;
		gridWidth = width;
		gr = new BoundedGrid<Actor>(gridLength,gridWidth);
	}
	
	protected void showWorld() {
		world = new ActorWorld(gr);
		world.show();
	}
		
	protected abstract void setUpEntites();

	@Override
	protected void setUpSound() {} //unnecessary

	@Override
	public void render() {} //unnecessary

	@Override
	public void input() {} //unnecessary

	@Override
	public void logic(int delta) {} //unnecessary

}
