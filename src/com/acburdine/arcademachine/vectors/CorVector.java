package com.acburdine.arcademachine.vectors;

public class CorVector extends TwoDVector {
	private float x,y;
	public CorVector() {
		x=y=0;
	}
	
	public CorVector(float ex, float why) {
		x=ex;
		y=why;
	}

	public void setX(float ex) {
		x=ex;

	}

	@Override
	public float getX() {
		return x;
	}

	public void setY(float why) {
		y=why;

	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public float getMagnitude() {
		return (float) Math.sqrt(x*x+y*y);
	}


	@Override
	public float getAngle() {
		float theta= (float) Math.atan(y/x);
		if(x>=0){
			if(y>=0){
				return theta;
			}else{
				return 2*PI+theta;
			}
		}else{
			return PI+theta;
		}
	}




}
