package com.acburdine.arcademachine.vectors;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.Sys;
import org.lwjgl.input.*;
import org.lwjgl.opengl.GL11;

import com.acburdine.arcademachine.Screen;

public class Floater implements Screen{
	TwoDVector pos;
	AngleVector speed;
	private long lastFrame, size=25;
	
	public Floater(){
		pos=new CorVector(100,100);
		speed = new AngleVector(0,0);
		lastFrame = getTime();
	}
	
	public Floater(float x,float y,float speed,float moveAngle) {
		pos=new CorVector(x,y);
		this.speed= new AngleVector(moveAngle, speed);
		lastFrame = getTime();
	}
	
	
	public void render() {
		glClear(GL_COLOR_BUFFER_BIT);
		logic(getDelta());
		AngleVector dleft= new AngleVector((float) (speed.getAngle()+Math.PI/2),size);
		AngleVector dright= new AngleVector((float) (speed.getAngle()-Math.PI/2),size);
		AngleVector dfront= new AngleVector((float) (speed.getAngle()),size);
		
		GL11.glColor3f(0.5f,0.5f,1.0f);
		GL11.glBegin(GL11.GL_TRIANGLES);
			GL11.glVertex2f(pos.getX(), pos.getY());
			GL11.glVertex2f(pos.getX()+dfront.getX()+dleft.getX(), pos.getY()+dfront.getY()+dleft.getY());
			GL11.glVertex2f(pos.getX()+dfront.getX()+dright.getX(), pos.getY()+dfront.getY()+dright.getY());
		GL11.glEnd();
	}
	private void logic(int delta) {
		pos=pos.add(speed);
		speed.setMagnitude((float) (speed.getMagnitude()*.9));
		if(speed.getMagnitude()<.01f)speed.setMagnitude(0);
		
	}


	public void input() {
		float a=0;
		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
			speed.setAngle((speed.getAngle()-0.06f));
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
			speed.setAngle((speed.getAngle()+0.06f));
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_UP)){
			a=.03f;
			//speed.setMagnitude(2);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)){
			//speed.setMagnitude(-1);
		}
		//System.out.println(speed);
		speed.setMagnitude(speed.getMagnitude()+(a*(20-speed.getMagnitude())));
	}
	
	private long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	private int getDelta() {
		long currentTime = getTime();
		
		int delta = (int) (currentTime - lastFrame);
		lastFrame = getTime();
		return delta;
	}
	
	

}
