package com.acburdine.arcademachine.vectors;

public class AngleVector extends TwoDVector {
	float theta, magnitude;
	public AngleVector() {
		theta=magnitude=0;
	}
	
	public AngleVector(float angle, float magnitude) {
		theta=angle;
		this.magnitude=magnitude;
	}

	
	@Override
	public float getX(){
		return (float) (magnitude*Math.cos(theta));
	}


	@Override
	public float getY() {
		return (float) (magnitude*Math.sin(theta));
	}

	public void setMagnitude(float m){
		magnitude=m;
	}
	@Override
	public float getMagnitude(){
		return magnitude;
	}

	public void setAngle(float a){
		theta=a;
	}
	@Override
	public float getAngle() {
		return theta;
	}

}
