package com.acburdine.arcademachine.vectors;

public abstract class TwoDVector {
	
	public final float PI= (float) Math.PI;

	
	abstract public float getX();
	
	
	abstract public float getY();
	
	abstract public float getAngle();
	
	abstract public float getMagnitude();
	
	public TwoDVector add(TwoDVector v){
		return new CorVector(this.getX()-v.getX(), this.getY()- v.getY());
	}
	
	public TwoDVector subtract(TwoDVector v){
		return new CorVector(this.getX()-v.getX(), this.getY()- v.getY());
	}
	
	public float cross(TwoDVector v){
		return this.getX()*v.getY() -this.getY()* v.getX();
	}
	
	public float dot(TwoDVector v){
		return this.getX()*v.getX()+ this.getY()* v.getY();
	}
	
	
	public String toString(){
		return "("+getX()+","+  getY() +")";
	} 
}
