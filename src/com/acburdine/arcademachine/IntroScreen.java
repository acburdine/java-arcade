package com.acburdine.arcademachine;

import static org.lwjgl.opengl.GL11.*;

import java.io.IOException;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.IFont;

/**
 * Introductory Screen that displays the logo
 * @author Austin Burdine
 *
 */
public class IntroScreen implements Screen {
	
	private Texture logo;
	private IFont font;
	private static final int WIDTH = 256;
	private static final int HEIGHT = 256;
	
	/**
	 * Loads the logo and font
	 */
	public IntroScreen() {
		glEnable(GL_TEXTURE_2D);
		try {
			logo = TextureLoader.getTexture("PNG", getClass().getResourceAsStream("logo.png"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		font = FontLoader.getFont("font_30");
	}

	/**
	 * Renders the screen
	 */
	public void render() {
		glClear(GL_COLOR_BUFFER_BIT);
		float zerox = (float) ((ArcadeMachine.WIDTH/2) - (WIDTH/2)), zeroy = (float) ((ArcadeMachine.HEIGHT/2) - (HEIGHT/2));
		logo.bind();
		glBegin(GL_QUADS);
		glTexCoord2f(0,0);
		glVertex2f(zerox, zeroy);
		glTexCoord2f(1,0);
		glVertex2f(zerox+WIDTH,zeroy);
		glTexCoord2f(1,1);
		glVertex2f(zerox+WIDTH, zeroy+HEIGHT);
		glTexCoord2f(0,1);
		glVertex2f(zerox, zeroy+HEIGHT);
		glEnd();
		glBindTexture(GL_TEXTURE_2D, font.getTextureID());
		String s = "Press Enter to continue.";
		font.getFont().drawString((float) ((ArcadeMachine.WIDTH/2)-(font.getFont().getWidth(s)/2)), zeroy+HEIGHT+20,s, Color.white);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	/**
	 * Checks keyboard input
	 */
	public void input() {
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
			ArcadeMachine.setState(new MasterMenu());
			glDisable(GL_TEXTURE_2D);
		}
	}

	
}
