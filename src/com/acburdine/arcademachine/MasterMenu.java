package com.acburdine.arcademachine;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.acburdine.arcademachine.entities.AbstractButton;
import com.acburdine.arcademachine.entities.Button;
import com.acburdine.arcademachine.entities.TexturedButton;
import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.GameEntry;
import com.acburdine.arcademachine.utils.GameLoader;
import com.acburdine.arcademachine.utils.IFont;
import com.acburdine.arcademachine.utils.Sound;
import com.acburdine.arcademachine.utils.SoundLoader;
import com.acburdine.brickbreaker.BrickBreaker;
import com.acburdine.papitramp.PapiTramp;
import com.acburdine.pong.PongMenu;
import com.acburdine.robotworld.RobotWorldMenu;
import com.acburdine.snake.SnakeMenu;

/**
 * Master Menu to navigate through all of the classes
 * @author Austin Burdine, Zach Steffen, Ariel Sandberg, Paul Sattizahn
 */
public class MasterMenu implements Screen {
	
	private Sound s;
	private Menu gameSelector;
	
	/**
	 * Initializes the buttons and loads the music
	 */
	public MasterMenu() {
		s = SoundLoader.getSound("intro");
		glEnable(GL_TEXTURE_2D);
		gameSelector = new Menu();
		try {
		    Thread.sleep(200);
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
		s.playSound();
	}

	/**
	 * Renders the menu
	 */
	public void render() {
		glClear(GL_COLOR_BUFFER_BIT);
		Color.white.bind();
		glRectd(0,0, ArcadeMachine.WIDTH, ArcadeMachine.HEIGHT);
		gameSelector.render();
	}

	/**
	 * Checks keyboard input
	 */
	public void input() {
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
			ArcadeMachine.kill();
		else if(Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			gameSelector.moveUp();
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			gameSelector.moveDown();
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
			gameSelector.selectCentered();
		}
	}
	
	private class MenuButton extends AbstractButton {
		
		private IFont font;
		private TrueTypeFont ttf;
		private boolean displayed = false;
		private GameEntry g;

		public MenuButton(double x, double y, double width, double height, GameEntry game, boolean display) {
			super(x, y, width, height);
			font = FontLoader.getFont("minecraftia_15");
			ttf = font.getFont();
			displayed = display;
			g = game;
		}

		@Override
		public void draw() {
			draw(Color.gray);
		}
		
		public void show() {
			displayed = true;
		}
		
		public void hide() {
			displayed = false;
		}
		
		public void draw(Color c) {
			if(displayed) {
				String txt = g.getName() + " " + g.getVersion();
				String txt2 = g.getDeveloper();
				c.bind();
				int txtWidth = ttf.getWidth(txt), txtHeight = ttf.getHeight(), txt2width = ttf.getWidth(txt2);
				glRectd(x,y,x+width,y+height);
				glBindTexture(GL_TEXTURE_2D, font.getTextureID());
				float x1 = (float) (x+((width/2)-(txtWidth/2))), x2 = (float)(x+((width/2)-(txt2width/2))), y1 = (float)(y+((height/2)-(txtHeight))), y2 = (float)(y+(height/2));
				ttf.drawString(x1,y1, txt, Color.black);
				ttf.drawString(x2, y2, txt2, Color.black);
				glBindTexture(GL_TEXTURE_2D, 0);
			}
		}
		
		public String getName() {
			return g.getName();
		}
		
		public boolean equals(MenuButton other) {
			return other.getName().equals(this.getName());
		}
		
		public void select() {
			s.stopSound();
			try {
				ArcadeMachine.setState((Screen) Class.forName(g.getGameClass()).newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	private class Menu {
		
		private ArrayList<MenuButton> buttons;
		private MenuButton centered;
		private TexturedButton upButton, downButton;
		private int mbi = 1;
		private static final int width = 350;
		private static final int height=350;
		private final int[][] options = {{(ArcadeMachine.WIDTH/2)-150, (ArcadeMachine.HEIGHT/2)-151, 300,100},{(ArcadeMachine.WIDTH/2)-150, (ArcadeMachine.HEIGHT/2)-50,300,100},{(ArcadeMachine.WIDTH/2)-150, (ArcadeMachine.HEIGHT/2)+51,300,100}};
		
		public Menu() {
			buttons = new ArrayList<MenuButton>();
			upButton = new TexturedButton((ArcadeMachine.WIDTH/2)-25, 0, 50, 50, getClass().getResourceAsStream("arrow.png"));
			downButton = new TexturedButton((ArcadeMachine.WIDTH/2)-25, 430, 50, 50, getClass().getResourceAsStream("arrow.png"));
			ArrayList<GameEntry> games = GameLoader.getLoadedGames();
			for(int i=0;i<games.size();i++) {
				if(i<options.length) {
					buttons.add(new MenuButton(options[i][0], options[i][1], options[i][2], options[i][3], games.get(i), true));
				}
				else
					buttons.add(new MenuButton(0,0,0,0,games.get(i), false));
			}
			centered = buttons.get(mbi);
		}
		
		public void render() {
			glBindTexture(GL_TEXTURE_2D, 0);
			upButton.draw();
			downButton.draw(true);
			upButton.update();
			downButton.update();
			Color.black.bind();
			int x1=(ArcadeMachine.WIDTH/2)-(width/2), y1=(ArcadeMachine.HEIGHT/2)-(height/2);
			glRecti(x1, y1, x1+width, y1+height);
			for(MenuButton b: buttons) {
				if(b.equals(centered)) {
					b.draw(Color.lightGray);
				} else {
					b.draw();
				}
				b.update();
				if(b.isLeftClick())
					b.select();
			}
			if(upButton.isLeftClick())
				moveUp();
			if(downButton.isLeftClick())
				moveDown();
		}
		
		public void moveDown() {
			if(mbi==0) {
				mbi++;
				centered = buttons.get(mbi);
			}
			else if(mbi<buttons.size()-2) {
				buttons.get(mbi-1).hide();
				mbi++;
				int count = 0;
				for(int i=mbi-1;i<=mbi+1;i++) {
					buttons.get(i).setX(options[count][0]);
					buttons.get(i).setY(options[count][1]);
					buttons.get(i).setWidth(options[count][2]);
					buttons.get(i).setHeight(options[count][3]);
					buttons.get(i).show();
					count++;
				}
				centered = buttons.get(mbi);
			} else if(mbi==buttons.size()-2) {
				mbi++;
				centered = buttons.get(mbi);
			}
		}
		
		public void moveUp() {
			if(mbi==buttons.size()-1) {
				mbi--;
				centered = buttons.get(mbi);
			}
			else if(mbi>1) {
				buttons.get(mbi+1).hide();
				mbi--;
				int count = 0;
				for(int i=mbi-1;i<=mbi+1;i++) {
					buttons.get(i).setX(options[count][0]);
					buttons.get(i).setY(options[count][1]);
					buttons.get(i).setWidth(options[count][2]);
					buttons.get(i).setHeight(options[count][3]);
					buttons.get(i).show();
					count++;
				}
				centered = buttons.get(mbi);
			} else if(mbi == 1){
				mbi--;
				centered = buttons.get(mbi);
			}
		}
		
		public void selectCentered() {
			centered.select();
		}
		
	}
	
}
