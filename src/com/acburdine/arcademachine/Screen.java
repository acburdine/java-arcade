package com.acburdine.arcademachine;

public interface Screen {
	
	/**
	 * Renders the screen
	 */
	public void render();
	
	/**
	 * Checks keyboard input
	 */
	public void input();

}
