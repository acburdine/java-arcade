package com.acburdine.arcademachine.utils;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;

/**
 * Wrapper class for Font Textures
 * @author Austin Burdine
 *
 */
public class IFont {
	
	private String path;
	private int id;
	private TrueTypeFont ttf;
	
	/**
	 * Creates a new font with the specified path and font size
	 * @param p - path to font
	 * @param textureID - Id that the texture will have
	 * @param fontSize - font size
	 */
	public IFont(String p, int textureID, float fontSize) {
		path = p;
		id = textureID;
		InputStream is = ResourceLoader.getResourceAsStream(path);
		Font f = null;
		try {
			f = Font.createFont(Font.TRUETYPE_FONT, is);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		f = f.deriveFont(fontSize);
		ttf = new TrueTypeFont(f, false);
	}
	
	/**
	 * Returns the TrueTypeFont texture associated with this font
	 * @return TrueTypeFont - font object associated with this font
	 */
	public TrueTypeFont getFont() {
		return ttf;
	}
	
	/**
	 * Returns the texture ID, used for glBindTexture and other such things
	 * @return int - The ID of the texture
	 */
	public int getTextureID() {
		return id;
	}
	
	/**
	 * Returns the path to the font
	 */
	public String toString() {
		return path;
	}

}
