package com.acburdine.arcademachine.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.apache.commons.io.FilenameUtils;

/**
 * Class for loading fonts
 * @author Austin Burdine
 */
public class FontLoader {
	
	private static HashMap<String, IFont> fonts = new HashMap<String, IFont>();
	
	/**
	 * Loads all of the fonts from the res/fonts folder, and creates IFont objects with those fonts
	 */
	public static void loadFonts() {
		int idCounter = 1;
		File fontsDir = new File("resources/fonts");
		for(File font: fontsDir.listFiles()) {
			String ext = FilenameUtils.getExtension(font.getPath());
			String s= FilenameUtils.getBaseName(font.getPath());
			if(ext.equals("ttf")) {
				Scanner sc = null;
				try {sc = new Scanner(new File("resources/fonts/"+s+".txt"));} catch (FileNotFoundException e) {e.printStackTrace();}
				ArrayList<Float> sizes = new ArrayList<Float>();
				while(sc.hasNext()) {
					sizes.add(Float.parseFloat(sc.nextLine()));
				}
				for(Float f: sizes) {
					String name = s+"_"+(int)f.floatValue();
					System.out.println(name);
					fonts.put(name, (new IFont(font.getPath(), idCounter, f)));
					idCounter++;
				}
			}
		}
	}
	
	/**
	 * Returns the IFont object associated with the fontname
	 * @param fontName - The name of the font and the size in this order: font_fontSize
	 * @return IFont - the font object
	 */
	public static IFont getFont(String fontName) {
		return fonts.get(fontName);
	}

}
