package com.acburdine.arcademachine.utils;

import static org.lwjgl.openal.AL10.*;

import org.newdawn.slick.openal.WaveData;
import org.newdawn.slick.util.ResourceLoader;

/**
 * Wrapper class for Sounds
 * @author Austin Burdine
 *
 */
public class Sound {
	
	private int buffer;
	private int source;
	
	/**
	 * Creates a new sound with the specified path
	 * @param path - Filepath of object
	 */
	public Sound(String path) {
		WaveData data = WaveData.create(ResourceLoader.getResourceAsStream(path));
		buffer = alGenBuffers();
		alBufferData(buffer, data.format, data.data, data.samplerate);
		data.dispose();
		source = alGenSources();
		alSourcei(source, AL_BUFFER, buffer);
	}
	
	/**
	 * Plays the sound
	 */
	public void playSound() {
		alSourcePlay(source);
	}
	
	/**
	 * Stops playing the sound
	 */
	public void stopSound() {
		alSourceStop(source);
	}
	
	/**
	 * Delets the buffer and source. Used for cleanup at the end of the running.
	 */
	public void removeSources() {
		alDeleteSources(source);
		alDeleteBuffers(buffer);
	}

}
