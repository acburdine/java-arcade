package com.acburdine.arcademachine.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.CodeSource;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class SetUpMachine {
	
	public static final String PREFIX = "stream2file";
    public static final String SUFFIX = ".tmp";

	public static boolean isFirstRun() {
		File f = new File("resources");
		return !f.exists();
	}
	
	public void runSetup() {
		File imagesDir = new File("resources/images");
		File fontsDir = new File("resources/fonts");
		File soundsDir = new File("resources/sounds");
		imagesDir.mkdirs();
		fontsDir.mkdirs();
		soundsDir.mkdirs();
		File gamesDir = new File("games");
		gamesDir.mkdirs();
		try {
			CodeSource src = SetUpMachine.class.getProtectionDomain().getCodeSource();
			if(src!=null) {
				URL jar = src.getLocation();
				ZipInputStream zip = new ZipInputStream(jar.openStream());
				for(ZipEntry e; (e = zip.getNextEntry()) != null;) {
					//initial check for resource files
					if(e.getName().startsWith("resources/")) {
						String resFileName = e.getName().substring(10);
						//check for image resources
						if(resFileName.startsWith("images/")) {
							String imageFileName = resFileName.substring(7);
							File f = new File(imagesDir, imageFileName);
							FileUtils.copyFile(stream2file(zip), f);
						}
						//check for fonts
						else if(resFileName.startsWith("fonts/")) {
							String fontFileName = resFileName.substring(6);
							File f = new File(fontsDir, fontFileName);
							FileUtils.copyFile(stream2file(zip), f);
						}
						//check for sounds
						else if(resFileName.startsWith("sounds/")) {
							String soundFileName = resFileName.substring(7);
							if(soundFileName.indexOf("/") != -1) {
								File dir = new File(soundsDir,soundFileName.substring(0, soundFileName.indexOf("/")));
								dir.mkdirs();
								File f = new File(dir, soundFileName.substring(soundFileName.indexOf("/")));
								FileUtils.copyFile(stream2file(zip), f);
							} else {
								File f = new File(soundsDir, soundFileName);
								FileUtils.copyFile(stream2file(zip), f);
							}
						}
					}
				}
				zip.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static File stream2file(InputStream in) throws IOException {
        final File tempFile = File.createTempFile(PREFIX, SUFFIX);
        tempFile.deleteOnExit();
        FileOutputStream out = new FileOutputStream(tempFile);
        IOUtils.copy(in, out);
        return tempFile;
    }
	
}
