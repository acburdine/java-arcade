package com.acburdine.arcademachine.utils;

import java.util.ArrayList;

public class GameLoader {
	
	private static ArrayList<GameEntry> games;
	
	public static void loadGames() {
		games = new ArrayList<GameEntry>();
		//hardcoded for now, will add dynamic games loading later
		games.add(new GameEntry("BrickBreaker", "1.0", "Paul Sattizahn", "com.acburdine.brickbreaker.BrickBreaker"));
		games.add(new GameEntry("Papi Tramp", "1.0", "Paul Sattizahn", "com.acburdine.papitramp.PapiTramp"));
		games.add(new GameEntry("Pong", "2.0", "Zach Steffen", "com.acburdine.pong.PongMenu"));
		games.add(new GameEntry("RobotWorld", "1.0", "Ariel Sandberg", "com.acburdine.robotworld.RobotWorldMenu"));
		games.add(new GameEntry("Snake", "1.0", "Paul Sattizahn", "com.acburdine.snake.SnakeMenu"));
	}
	
	public static ArrayList<GameEntry> getLoadedGames() {
		return games;
	}

}
