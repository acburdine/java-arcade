package com.acburdine.arcademachine.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.FilenameUtils;

/**
 * Class to handle sound loading
 * @author Austin Burdine
 *
 */
public class SoundLoader {
	
	private static HashMap<String, Sound> sounds = new HashMap<String, Sound>();
	private static ArrayList<String> soundNames = new ArrayList<String>();
	
	/**
	 * Handles subdirectories. Wrapper for the actual loadSounds method
	 */
	public static void loadSounds() {
		File soundsDir = new File("resources/sounds");
		for(File sound: soundsDir.listFiles()) {
			if(sound.isDirectory()) {
				for(File subLevelSound: sound.listFiles()) {
					loadSound(subLevelSound, true);
				}
			} else {
				loadSound(sound, false);
			}
		}
	}
	
	/**
	 * Loads all of the sound files from disk and creates them as Sound objects
	 * @param f - File that contains the sound
	 * @param isSubDir - if the file is in a subdirectory of the res/sounds folder
	 */
	private static void loadSound(File f, boolean isSubDir) {
		String s = "";
		if(isSubDir)
			s += FilenameUtils.getBaseName(f.getParent()) + "_";
		s += FilenameUtils.getBaseName(f.getPath());
		System.out.println(s);
		soundNames.add(s);
		sounds.put(s, new Sound(f.getPath()));
	}
	
	/**
	 * Returns the sound file associated with the name
	 * @param name - the name of the sound
	 * @return Sound - the sound object
	 */
	public static Sound getSound(String name) {
		return sounds.get(name);
	}
	
	/**
	 * Deletes all of the sound resources. Used for object cleanup
	 */
	public static void deleteSources() {
		for(String s: soundNames)
			sounds.get(s).removeSources();
	}

}
