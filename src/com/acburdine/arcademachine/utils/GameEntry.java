package com.acburdine.arcademachine.utils;

public class GameEntry {

	private String name, version, developer, className;
	
	public GameEntry(String n, String v, String d, String cName) {
		name = n;
		version = v;
		className = cName;
		developer = d;
	}
	
	public String getGameClass() {
		return className;
	}
	
	public String getName() {
		return name;
	}
	
	public String getVersion() {
		return version;
	}
	
	public String getDeveloper() {
		return developer;
	}
	
	
}
