package com.acburdine.arcademachine.entities;

import java.awt.Rectangle;

/**
 * Abstract class for non-moveable entities
 * @author Austin Burdine
 */
public abstract class AbstractEntity implements Entity {

	protected double x,y,width,height;
	protected Rectangle hitbox = new Rectangle() ;
	
	/**
	 * Constructs a box (rectangular shaped)
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - width of box
	 * @param height - height of box
	 */
	public AbstractEntity(double x, double y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/**
	 * Sets the initial x and y coordinates of the box
	 * @param x - New initial x coordinate
	 * @param y - New initial y coordinate
	 */
	public void setLocation(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Sets the X coordinate
	 * @param x - New initial x coordinate
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Sets the Y coordinate
	 * @param y - New initial y coordinate
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Sets a new width for the box
	 * @param width - New width of the box
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * Sets a new height for the box
	 * @param height - New height of the box
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 * Gets the initial x coordinate
	 * @return double - X coordinate
	 */
	public double getX() {
		return x;
	}

	/**
	 * Gets the initial y coordinate
	 * @return double - Y coordinate
	 */
	public double getY() {
		return y;
	}

	/**
	 * Gets the width
	 * @return double - Width of box
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * Gets the height
	 * @return double - Height of box
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * Checks if another entity is intersecting this one
	 * @param other - Entity you want to check
	 * @return boolean - true if entities intersect; otherwise false
	 */
	public boolean intersects(Entity other) {
		hitbox.setBounds((int) x, (int) y, (int) width, (int) height);
		return hitbox.intersects(other.getX(), other.getY(), other.getWidth(), other.getHeight());
	}
	
	/**
	 * Checks if the position (x,y) is within the bounds of the box. (Mostly used for mouse positioning)
	 * @param x - X coordinate
	 * @param y - Y coordinate
	 * @return boolean - true if (x,y) is within the box; otherwise false
	 */
	public boolean isHoveringOver(double x, double y) {
		return (x>=getX() && x<=getX()+getWidth() && y>=getY() && y<=getY()+getHeight());
	}
}
