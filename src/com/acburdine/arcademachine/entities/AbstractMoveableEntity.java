package com.acburdine.arcademachine.entities;


/**
 * Abstract class for moveable entities;
 * @author Austin Burdine
 *
 */
public abstract class AbstractMoveableEntity extends AbstractEntity implements MoveableEntity {
	
	protected double dx, dy;

	/**
	 * Constructs a new Moveable Entity
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public AbstractMoveableEntity(double x, double y, double width, double height) {
		super(x, y, width, height);
		this.dx = 0;
		this.dy = 0; 
	}
	
	/**
	 * Moves the object by the set dx and dy times the delta (time difference between frames)
	 * @param delta - Time difference between frames
	 */
	public void update(int delta) {
		this.x += delta*dx;
		this.y += delta*dy;
	}
	
	/**
	 * Gets the current x multiplier
	 * @return double - X multiplier
	 */
	public double getDX() {
		return this.dx;
	}
	
	/**
	 * Gets the current y multiplier
	 * @return double - Y multiplier
	 */
	public double getDY() {
		return this.dy;
	}
	
	/**
	 * Sets the x multiplier
	 * @param dx - new X multiplier
	 */
	public void setDX(double dx) {
		this.dx = dx;
	}
	
	/**
	 * Sets the y multiplier
	 * @param dy - new Y multipier
	 */
	public void setDY(double dy) {
		this.dy = dy;
	}
	
}
