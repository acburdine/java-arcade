package com.acburdine.arcademachine.entities;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

/**
 * A ball with a texture (this texture is located in the /res folder
 * @author Paul Sattizahn
 */
public class BallTextured extends Ball {
	
	private Texture ballTexture;

	/**
	 * Creates a textured ball
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - Width of box
	 * @param height - Height of box
	 */
	public BallTextured(double x, double y, double width, double height) {
		super(x, y, width, height);
		GL11.glEnable(GL11.GL_TEXTURE_2D); 
		try {
			// load texture from PNG file
			ballTexture = TextureLoader.getTexture("PNG", getClass().getResourceAsStream("ball.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Draws the textured ball
	 */
	public void draw() {
		glBindTexture(GL_TEXTURE_2D, ballTexture.getTextureID());
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0,0);
		GL11.glVertex2f((float)getX(),(float)getY());
		GL11.glTexCoord2f(1,0);
		GL11.glVertex2f((float)(getX()+getWidth()),(float)getY());
		GL11.glTexCoord2f(1,1);
		GL11.glVertex2f((float)(getX()+getWidth()),(float)(getY()+getHeight()));
		GL11.glTexCoord2f(0,1);
		GL11.glVertex2f((float)getX(),(float)(getY()+getHeight()));
		GL11.glEnd();
	}

}
