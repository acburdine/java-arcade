package com.acburdine.arcademachine.entities;

import static org.lwjgl.opengl.GL11.*;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.acburdine.arcademachine.utils.FontLoader;
import com.acburdine.arcademachine.utils.IFont;

/**
 * Basic clickable button class
 * @author Austin Burdine
 */
public class Button extends AbstractButton {
	
	protected IFont font;
	protected TrueTypeFont ttf;
	protected String defaultText;
	protected Color basic,text;
	
	/**
	 * Constructs a basic button without specified text or color.
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - Width of button
	 * @param height - Height of button
	 */
	public Button(double x, double y, double width, double height) {
		super(x,y,width,height);
		font = FontLoader.getFont("minecraftia_24");
		ttf = font.getFont();
	}
	
	/**
	 * Constructs a more specific button with default text, font height, background color, and text color.
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - Width of button
	 * @param height - Height of button
	 * @param fontSize - Font size (analogous to regular font sizes)
	 * @param fontName - Name of font
	 * @param defaultText - Default string to be drawn on the button
	 * @param baseColor - Default background color for button
	 * @param textColor - Default text color
	 */
	public Button(double x, double y, double width, double height, int fontSize, 
			String fontName, String defaultText, Color baseColor, Color textColor) {
		super(x, y, width, height);
		font = FontLoader.getFont(fontName+"_"+fontSize);
		ttf = font.getFont();
		this.defaultText = defaultText;
		basic = baseColor;
		text = textColor;
	}
	
	/**
	 * Draws a button with the default text, text color and background color.
	 */
	public void draw() {
		draw(defaultText, basic);
	}

	/**
	 * Draws a more specified button with the specified txt and background color
	 * @param txt - Text to place on the button
	 * @param bindColor - Background color of button
	 */
	public void draw(String txt, Color bindColor) {
		glBindTexture(GL_TEXTURE_2D, 0);
		bindColor.bind();
		int txtWidth = ttf.getWidth(txt), tempWidth = (int) width, txtHeight = ttf.getHeight(), tempHeight = (int) height;
		if(txtWidth > tempWidth)
			tempWidth = txtWidth;
		if(txtHeight > tempHeight)
			tempHeight = txtHeight;
		glRectd(x,y,x+tempWidth,y+tempHeight);
		glBindTexture(GL_TEXTURE_2D, font.getTextureID());
		float x1 = (float) (x+((width/2)-(txtWidth/2))), y1 = (float)(y+((height/2)-(txtHeight/2)));
		ttf.drawString(x1,y1, txt, text);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}
