package com.acburdine.arcademachine.entities;


/**
 * Moveable Entity - moves based on the updates in system time multiplied by values dx and dy
 * @author Austin Burdine
 */
public interface MoveableEntity extends Entity {
	
	/**
	 * Gets the mutiplier value of x
	 * @return double - x-multiplier value
	 */
	public double getDX();
	
	/**
	 * Gets the multiplier value of y
	 * @return double - y-multiplier value
	 */
	public double getDY();
	
	/**
	 * Sets the x-mutiplier value
	 * @param dx - the new x-multiplier value
	 */
	public void setDX(double dx);
	
	/**
	 * Sets the y-multiplier value
	 * @param dy - the new y-multiplier value
	 */
	public void setDY(double dy);
}
