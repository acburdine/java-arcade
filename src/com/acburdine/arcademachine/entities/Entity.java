package com.acburdine.arcademachine.entities;

/**
 * Interface for creating Entities in the ArcadeMachine game
 * @author Austin Burdine
 */
public interface Entity {
	
	/**
	 * Draws the Entity object
	 */
	public void draw();
	
	/**
	 * Used for Moveable Entities, updates the x and y positions based on the change in system time since the last frame
	 * @param delta - Change in system time since the last frame
	 */
	public void update(int delta);
	
	/**
	 * Sets the new initial coordinate of the entity
	 * @param x - New initial x coordinate
	 * @param y - New initial y coordinate
	 */
	public void setLocation(double x,double y);
	
	/**
	 * Sets a new initial X coordinate
	 * @param x - New initial x coordinate
	 */
	public void setX(double x);
	
	/**
	 * Sets a new initial Y coordinate
	 * @param y - New initial y coordinate
	 */
	public void setY(double y);
	
	/**
	 * Sets a new width for the entity
	 * @param width - New width
	 */
	public void setWidth(double width);
	
	/**
	 * Sets a new height for the entity
	 * @param height - New height
	 */
	public void setHeight(double height);
	
	/**
	 * Gets the initial x coordinate
	 * @return double - x coordinate
	 */
	public double getX();
	
	/**
	 * Gets the initial y coordinate
	 * @return double - y coordinate
	 */
	public double getY();
	
	/**
	 * Gets the width
	 * @return double - width
	 */
	public double getWidth();
	
	/**
	 * Gets the height
	 * @return double - height
	 */
	public double getHeight();
	
	/**
	 * Checks if one entity is intersecting another
	 * @param other - Another entity
	 * @return boolean - True if the two are intersecting, false if not.
	 */
	public boolean intersects(Entity other);
	
	/**
	 * Checks if the position (x,y) is over the entity
	 * @param x - X coordinate
	 * @param y - Y coordinate
	 * @return boolean - True if the point (x,y) is within the bounds of the entity; otherwise false
	 */
	public boolean isHoveringOver(double x, double y);
}
