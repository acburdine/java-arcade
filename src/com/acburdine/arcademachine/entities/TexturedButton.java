package com.acburdine.arcademachine.entities;

import static org.lwjgl.opengl.GL11.*;

import java.io.IOException;
import java.io.InputStream;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

/**
 * Textured button for creating buttons with textures instead of (or in conjunction with) background colors.
 * @author Austin Burdine
 */
public class TexturedButton extends AbstractButton {
	
	private Texture texture;
	
	/**
	 * Constructs a simple textured button with the "button_grey" texture
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - Initial width
	 * @param height - Initial height
	 */
	public TexturedButton(double x, double y, double width, double height) {
		super(x,y,width,height);
		try {
			texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/button_grey.png"));
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Consructs a more specified button.
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - Width of button
	 * @param height - Height of button
	 * @param fontSize - Size of font
	 * @param fontName - Name of font
	 * @param defaultString - Default text for button
	 * @param baseColor - Default background color
	 * @param textColor - Default text color
	 * @param texturePath - Path to the texture you want to bind to the button
	 */
	public TexturedButton(double x, double y, double width, double height, InputStream textureStream) {
		super(x, y, width, height);
		glEnable(GL_TEXTURE_2D);
		try {
			texture = TextureLoader.getTexture("PNG", textureStream);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void draw() {
		texture.bind();
		glBegin(GL_QUADS);
		GL11.glTexCoord2f(0,0);
		GL11.glVertex2f((float)getX(),(float)getY());
		GL11.glTexCoord2f(1,0);
		GL11.glVertex2f((float)(getX()+getWidth()),(float)getY());
		GL11.glTexCoord2f(1,1);
		GL11.glVertex2f((float)(getX()+getWidth()),(float)(getY()+getHeight()));
		GL11.glTexCoord2f(0,1);
		GL11.glVertex2f((float)getX(),(float)(getY()+getHeight()));
		GL11.glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public void draw(boolean rotate) {
		if(rotate) {
			glPushMatrix();
			glTranslatef((float)(x+width/2),(float)(y+height/2), 0);
			glRotatef(180,0,0,1);
			float width2 = (float) (width/2), height2 = (float)(height/2);
			texture.bind();
			glBegin(GL_QUADS);
			GL11.glTexCoord2f(0,0);
			GL11.glVertex2f(-width2,-height2);
			GL11.glTexCoord2f(1,0);
			GL11.glVertex2f(width2,-height2);
			GL11.glTexCoord2f(1,1);
			GL11.glVertex2f(width2,height2);
			GL11.glTexCoord2f(0,1);
			GL11.glVertex2f(-width2,height2);
			GL11.glEnd();
			glBindTexture(GL_TEXTURE_2D, 0);
			glPopMatrix();
		} else {
			draw();
		}
	}
}
