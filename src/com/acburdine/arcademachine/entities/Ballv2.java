package com.acburdine.arcademachine.entities;


import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

/**
 * Movable Ball entity (Not textured)
 * @author Paul Sattizahn
 *
 */
public class Ballv2 extends AbstractMoveableEntity {
	
	protected final int QUALITY=30;
	protected Color color;
	protected double radius;

	/**
	 * Creates a new Ball entity
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 */
	public Ballv2(double x, double y, double radius) {
		super(x, y, radius*1.7, radius*1.7);
		color=Color.white;
		this.radius=radius;
	}

	/**
	 * Draws the ball
	 */
	public void draw() {
		double centerX=x+width/2;
		double centerY=y+height/2;
		color.bind();
		GL11.glBegin(GL11.GL_TRIANGLE_FAN);
		GL11.glVertex2d(centerX, centerY);
		for(int i = 0; i <= QUALITY; i++){ //QUALITY decides how round the circle looks.
		    double angle = Math.PI * 2 * i / QUALITY;
		    GL11.glVertex2d(centerX+Math.cos(angle)*radius,centerY+ Math.sin(angle)*radius);
		}
		GL11.glEnd();
		
	}
	

	/**
	 * Sets color to given color
	 * @param c- input color
	 */
	public void setColor(Color c){
		color=c;
	}
	
	
	/**
	 * Checks if a point is inside this 
	 * @param X - Point's x you want to check
	 * @param Y - Point's y you want to check
	 * @return boolean - true if entities intersect; otherwise false
	 */
	public boolean intersects(double pointX, double pointY) {
		double disX=x-pointX, disY=y-pointY;
		return radius*radius>disX*disX+disY*disY;
	}

	
	
}
