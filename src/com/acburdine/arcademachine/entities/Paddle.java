package com.acburdine.arcademachine.entities;

import static org.lwjgl.opengl.GL11.*;

/**
 * Paddle class for use in Pong game or other games. It's a basic moveable rectangle
 * @author Austin Burdine
 */
public class Paddle extends AbstractMoveableEntity {

	/**
	 * Constructs a new Paddle (Rectangle) with the specified parameters
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - Width of paddle
	 * @param height - Height of paddle
	 */
	public Paddle(double x, double y, double width, double height) {
		super(x, y, width, height);
	}

	/**
	 * Draws the paddle
	 */
	
	public void draw() {
		glRectd(x,y,x+width,y+height);
	}

}
