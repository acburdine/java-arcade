package com.acburdine.arcademachine.entities;

import static org.lwjgl.opengl.GL11.glRectd;

/**
 * Moveable Ball entity (Not textured)
 * @author Paul Sattizahn
 *
 */
public class Ball extends AbstractMoveableEntity {

	/**
	 * Creates a new Ball entity
	 * @param x - Initial x coordinate
	 * @param y - Initial y coordinate
	 * @param width - Width of ball
	 * @param height - height of ball
	 */
	public Ball(double x, double y, double width, double height) {
		super(x, y, width, height);
	}

	/**
	 * Draws the ball
	 */
	public void draw() {
		glRectd(x,y,x+width,y+height);
	}
	
}
