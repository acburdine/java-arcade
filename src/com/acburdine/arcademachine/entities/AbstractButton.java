package com.acburdine.arcademachine.entities;

import org.lwjgl.input.Mouse;

import com.acburdine.arcademachine.ArcadeMachine;

public abstract class AbstractButton extends AbstractEntity{
	
	protected boolean isLeftClick, isRightClick, isHovering, clicked, persistentClick;
	
	public AbstractButton(double x, double y, double width, double height) {
		super(x,y,width,height);
		isLeftClick = false;
		isRightClick = false;
		isHovering = false;
		clicked = false;
		persistentClick = false;
		fixMouseClickPersistency();
	}
	
	private void fixMouseClickPersistency() {
		if(Mouse.isButtonDown(0) || Mouse.isButtonDown(1)) {
			persistentClick = true;
		}
	}
	
	/**
	 * If the button has been left-clicked
	 * @return - True if the button has been left-clicked; otherwise false
	 */
	public boolean isLeftClick() {
		return isLeftClick;
	}
	
	/**
	 * If the button has been right-clicked
	 * @return - True if the button has been right-clicked; otherwise false
	 */
	public boolean isRightClick() {
		return isRightClick;
	}
	
	/**
	 * If the mouse is currently hovering over the button
	 * @return - True if the mouse is currently hovering over the button; otherwise false
	 */
	public boolean isHovering() {
		return isHovering;
	}
	
	/**
	 * If the button has been clicked (used for background color purposes)
	 * @return - True if the button has (or is still being) clicked; otherwise false
	 */
	public boolean hasBeenClicked() {
		return clicked;
	}
	
	/**
	 * Updates all of the clicking booleans
	 */
	public void update() {
		if(isHoveringOver(ArcadeMachine.getMouseX(), ArcadeMachine.getMouseY())) {
			isHovering = true;
			if(Mouse.isButtonDown(0) && !persistentClick) {
				isLeftClick = !clicked;
				clicked = true;
			} else if(Mouse.isButtonDown(1) && !persistentClick) {
				isRightClick = !clicked;
				clicked = true;
			} else {
				clicked = false;
				persistentClick = (Mouse.isButtonDown(0) || Mouse.isButtonDown(1));
			}
		} else {
			isHovering = false;
		}
	}
	
	public void update(int delta) {} //unnecessary
	
	public abstract void draw();
	
	

}
