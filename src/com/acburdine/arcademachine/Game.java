package com.acburdine.arcademachine;


/**
 * Abstract class for all of the game running classes
 * @author Paul Sattizahn / Austin Burdine
 *
 */
public abstract class Game implements Screen{

	protected long lastFrame;
	
	/**
	 * Constructs a new Game with the timer, sound, and entities
	 */
	public Game() {
		setUpTimer();
		setUpSound();
		setUpEntites();
	}
	
	/**
	 * Sets up the individual game entites
	 */
	protected abstract void setUpEntites();
	
	/**
	 * Sets up the individual game sounds
	 */
	protected abstract void setUpSound();
	
	/**
	 * Renders the game
	 */
	public abstract void render();
	
	/**
	 * Checks inputs of the game
	 */
	public abstract void input();
	
	/**
	 * Performs the game logic
	 * @param delta - Amount of system time change since the last frame
	 */
	public abstract void logic(int delta);
	
	/**
	 * Sets up the frame for game logic purposes
	 */
	protected void setUpTimer() {
		lastFrame = ArcadeMachine.getTime();
	}
	
	
	

}
