## How to Run the Game

1. Go to Downloads
2. Download the jar version that corresponds to your operating system (windows,mac,or linux)
3. Place the jar in its own directory (e.g. "ArcadeMachine")
4. Click to run!

####NOTE: I didn't test the Windows/Linux versions, but they should work.